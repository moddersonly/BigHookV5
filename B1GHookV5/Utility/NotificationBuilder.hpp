#pragma once
#include "Notification.hpp"

class NotificationBuilder
{
public:
	virtual ~NotificationBuilder() {};

	Notification* getNotification()
	{
		return m_Notification.release();
	}
	virtual NotificationBuilder* createNotification()
	{
		m_Notification = std::make_unique<Notification>();
		return this;
	}
	virtual NotificationBuilder* addDict(const std::string&) = 0;
	virtual NotificationBuilder* addTex(const std::string&) = 0;
	virtual NotificationBuilder* addTitle(const std::string&) = 0;
	virtual NotificationBuilder* addSubtitle(const std::string&) = 0;
	virtual NotificationBuilder* addClanTag(const std::string&) = 0;
	virtual NotificationBuilder* addDescription(const std::string&) = 0;
	virtual NotificationBuilder* addType() = 0;
	virtual void notify()
	{
		m_Notification->open();
	}
protected:
	std::unique_ptr<Notification> m_Notification;
};
