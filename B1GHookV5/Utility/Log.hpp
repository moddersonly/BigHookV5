#ifndef LOG_H
#define LOG_H
#ifdef __DEBUG
#define DEBUGOUT( X, ... ) Logger::Debug( X, __VA_ARGS__ )
#else
#define DEBUGOUT( X, ... )
#endif

#if _DEBUG
#define DEBUGMSG CLog::msg
#else
#define DEBUGMSG //
#endif
#include <vector>
#include <cstdarg>

struct RawStringStreamBuf : public std::streambuf
{
	RawStringStreamBuf(char* array, std::size_t size)
	{
		std::streambuf::setp(array, array + size - 1);
		std::memset(array, 0, size);
	}

	template <std::size_t size>
	RawStringStreamBuf(char(&array)[size]) :
		RawStringStreamBuf(&array[0], size)
	{
	}
};

struct RawStringStream : virtual RawStringStreamBuf, public std::ostream
{
	RawStringStream(char* array, std::size_t size) :
		RawStringStreamBuf(array, size),
		std::ostream(this)
	{
	}

	template <std::size_t size>
	RawStringStream(char(&array)[size]) :
		RawStringStreamBuf(array),
		std::ostream(this)
	{
	}
};

class Logger
{
private:
	std::deque<std::unique_ptr<char[]>> m_Logs{};

	std::unique_ptr<std::ofstream> m_Console = nullptr;
	std::unique_ptr<std::ofstream> m_FileStream = nullptr;
	char m_LogFile[MAX_PATH] = {};
public:
	Logger() = default;

	/**
     * \brief Creates a file for logging
     * \param szDir Directory for file
     * \return true/false
     */
    bool initialize(const char* dirName);
	void uninitialize();
	/**
	 * \brief adds a message to the log
	 * \param format Message to log.
	 */
	void msg(const char* format, ...);
	/**
	 * \brief adds a message to the log as an error
	 * \param format Error
	 */
	void error(const char* format, ...);
	/**
	 * \brief Adds a message to the log and crashes game
	 * \param format Fatal error
	 */
	void fatal(const char* format, ...);

	auto const& getLog()
	{
		return m_Logs;
	}
protected:
	std::unique_ptr<char[]> log(const char* const prefix, const char* const format, std::va_list args);
};
#endif // LOG_H
