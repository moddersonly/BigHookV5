#include "pch.hpp"
#include "ClanTagNotificationBuilder.hpp"


ClanTagNotificationBuilder* ClanTagNotificationBuilder::createNotification()
{
	m_Notification = std::make_unique<Notification>();
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addDescription(const std::string& desc)
{
	m_Notification->setDescription(desc);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addType()
{
	m_Notification->setType(eNotificationType::eClan1);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addDict(const std::string& dict)
{
	m_Notification->setDict(dict);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addTex(const std::string& tex)
{
	m_Notification->setTexture(tex);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addTitle(const std::string& title)
{
	m_Notification->setTitle(title);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addSubtitle(const std::string& sub)
{
	m_Notification->setSubTItle(sub);
	return this;
}

ClanTagNotificationBuilder* ClanTagNotificationBuilder::addClanTag(const std::string& clan)
{
	m_Notification->setClanTag(clan);
	return this;
}
