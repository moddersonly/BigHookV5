#include "pch.hpp"
#include "ScriptGlobal.hpp"

ScriptGlobal::ScriptGlobal(void* addr):
	m_Addr(addr)
{
}

ScriptGlobal::ScriptGlobal(const std::uintptr_t index):
	m_Addr(&(ClassPointers::g_ScriptThread->globalPtr()[index >> 0x12 & 0x3F][index & 0x3FFFF]))
{
}

ScriptGlobal ScriptGlobal::operator [](const std::uintptr_t index) const
{
	return ScriptGlobal(reinterpret_cast<void**>(m_Addr) + index);
}

void * ScriptGlobal::operator&() const
{
	return m_Addr;
}


ScriptGlobal operator ""_Global(const std::uintptr_t index)
{
	return ScriptGlobal(index);
}

ScriptGlobal operator ""_Tunable(const std::uintptr_t index)
{
	return ScriptGlobal(0x40001 + index);
}

ScriptGlobal operator ""_PlayerInfo(const std::uintptr_t playerId)
{
	return ScriptGlobal(1589291 + 1 + (playerId * 770) + 211);
}
