#include "pch.hpp"
#include "ScriptEvent.hpp"

RemoteScriptEvent::RemoteScriptEvent(std::uint64_t eventId, const std::uint32_t player)
{
	m_Args = { eventId, player, 0, 0 };
	m_Bitset = 1 << player;
}
