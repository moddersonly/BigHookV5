#pragma once
#include "Notification.hpp"
#include "NotificationBuilder.hpp"

class RegularNotificationBuilder : public NotificationBuilder
{
public:
	virtual ~RegularNotificationBuilder() = default;;

	RegularNotificationBuilder* createNotification() override;

	RegularNotificationBuilder* addDescription(const std::string& desc) override;

	RegularNotificationBuilder* addType() override;

	RegularNotificationBuilder* addDict(const std::string&) override;

	RegularNotificationBuilder* addTex(const std::string&) override;

	RegularNotificationBuilder* addTitle(const std::string&) override;

	RegularNotificationBuilder* addSubtitle(const std::string&) override;

	RegularNotificationBuilder* addClanTag(const std::string&) override;
};
