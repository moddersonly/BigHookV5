#include "pch.hpp"
#include "ScriptBlip.hpp"


ScriptBlip::ScriptBlip(CBlip* blip):
	m_Blip(blip)
{
}

Vector3_t ScriptBlip::getPosition() const
{
	return m_Blip->coords;
}

ScriptBlip& ScriptBlip::setPosition(Vector3_t position)
{
	m_Blip->coords = position;
	return *this;
}

float ScriptBlip::getScale() const
{
	return m_Blip->fScale;
}

ScriptBlip& ScriptBlip::setScale(const float scale)
{
	m_Blip->fScale = scale;
	return *this;
}

char* ScriptBlip::getName() const
{
	return m_Blip->szMessage;
}

ScriptBlip& ScriptBlip::setName(const char* name)
{
	m_Blip->szMessage = const_cast<char*>(name);
	return *this;
}

bool ScriptBlip::isFocused() const
{
	return m_Blip->bFocused & 1 << 7;
}

ScriptBlip& ScriptBlip::setFocused(const bool focused)
{
	if (focused)
	{
		m_Blip->bFocused |= 1 << 7;
	}
	else
	{
		m_Blip->bFocused &= ~(1 << 7);
	}

	return *this;
}

eBlipSprite ScriptBlip::getIcon() const
{
	return static_cast<eBlipSprite>(m_Blip->iIcon);
}

ScriptBlip& ScriptBlip::setIcon(const eBlipSprite icon)
{
	m_Blip->iIcon = static_cast<int32_t>(icon);
	return *this;
}

tBlipRotation ScriptBlip::getRotation() const
{
	return m_Blip->iRotation;
}

ScriptBlip& ScriptBlip::setRotation(tBlipRotation rotation)
{
	m_Blip->iRotation = rotation;
	return *this;
}
 
tBlipAlpha ScriptBlip::getAlpha() const
{
	return m_Blip->bAlpha;
}

ScriptBlip& ScriptBlip::setAlpha(tBlipAlpha alpha)
{
	m_Blip->bAlpha = alpha;
	return *this;
}

IColor4 ScriptBlip::getColor() const
{
	return IColor4(m_Blip->dwColor);
}

ScriptBlip& ScriptBlip::setColor(IColor4 color)
{
	m_Blip->dwColor = color.full;
	return *this;
}
