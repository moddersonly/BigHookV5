#pragma once
#include "BigVec2.hpp"

class DrawManager
{
    int m_TextNumber;
    int m_RectNumber;
    int m_LineNumber;
    int m_PolyNumber;
    int m_SpriteNumber;
public:
    DrawManager();
    ~DrawManager();
    void drawText(const char *text, std::int64_t font, CColor4 color, BigVec2 pos, BigVec2 scale,
                  std::uint16_t alignment,
        BigVec2 wrap = BigVec2(0.f, 1920.f), bool outline = false, bool shadow = false);
    void drawRect(CColor4 color, BigVec2 pos, BigVec2 scale);
    void drawLine(CColor4 color, Vector3_t pos, Vector3_t pos2);
    void drawPolygon(CColor4 color, Vector3_t pos, Vector3_t pos2, Vector3_t pos3);
    void drawSprite(const char *dict, const char *tex, BigVec2 pos, BigVec2 size, CColor4 color, float rot);

    decltype(auto) texxNum() const { return m_TextNumber; }
    decltype(auto) rectNum() const { return m_RectNumber; }
    decltype(auto) lineNum() const { return m_LineNumber; }
    decltype(auto) polyNum() const { return m_PolyNumber; }
    decltype(auto) spriteNum() const { return m_SpriteNumber; }

    void clearNums();
};

