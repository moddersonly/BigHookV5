#ifndef SCRIPTGLOBAL_HPP
#define SCRIPTGLOBAL_HPP
#include "pch.hpp"

class ScriptGlobal
{
	void* m_Addr = nullptr;
public:
	explicit ScriptGlobal(void* addr);
	explicit ScriptGlobal(std::uintptr_t index);

	ScriptGlobal operator [](std::uintptr_t index) const;

	template <typename T>
	std::enable_if_t<std::is_pointer_v<T>, T> as() const
	{
		return reinterpret_cast<T>(m_Addr);
	}

	template <typename T>
	std::enable_if_t<std::is_lvalue_reference_v<T>, T> as() const
	{
		return *reinterpret_cast<std::add_pointer_t<std::remove_reference_t<T>>>(m_Addr);
	}

	template <typename T>
	std::enable_if_t<std::is_fundamental_v<T>, T> as() const
	{
		return as<std::add_lvalue_reference_t<T>>();
	}

	void* operator &() const;
};

ScriptGlobal operator ""_Global(std::uintptr_t index);
ScriptGlobal operator ""_Tunable(std::uintptr_t index);
ScriptGlobal operator ""_PlayerInfo(std::uintptr_t index);

//ScriptGlobal playerGlobal(std::uintptr_t playerId);
#endif // SCRIPTGLOBAL_HPP
