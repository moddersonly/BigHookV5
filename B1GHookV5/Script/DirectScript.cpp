#include "pch.hpp"
#include "DirectScript.hpp"
#include "DirectX/ImGUi/imgui_impl_dx11.h"
#include "DirectX/ImGUi/imgui_impl_win32.h"


DirectScript::DirectScript()
{
}

DirectScript::~DirectScript()
{
}

void DirectScript::initialize()
{

}

// File: 'vibra.ttf' (50616 bytes)
// Exported using binary_to_compressed_c.cpp
static const char vibrab85[41530 + 1] =
"7])#######LH_C_'/###S),##2(V$#Q6>##u%1S:t@rT.?oi=c4gj/1RZn42R#:p+g->>#Y@^01aNV=Bx5^LVjI:;$QME/1[1XGHb%u.#=*35&+MD21$Jk7D7(88Udumo%c%wX.(a(*H"
"Z%vhLiqV7Y2tiW%,5LsC)w,XGSN)'u5Tdg%b';9C'tWC2Rd[w'9lrLDK.xA,+wS%M5V$lL@x$KD.8e)%kmQtLGE):#6-tqE]qwa$F6X,#<'N##;Pa.FpDYf#6lsjL)/moLbINDFAa-$o"
"`((-#vTbw'^k/3GPm7;L1qH=#^qS*MZrW5Gi6A.u8Be:#X@@k=)g?QMfu'-1a/UW_odPir*e=Snf#e&#_Ou$M0X$##EIi9?^P`@s<A/,M;tZv$U1FcMrwSfL6Hb8v;DX*$H0U&nrFqSM"
"=b@fMw)/YMW6'5#pAXGMjlbkLUn)$#`gmA#<<.6/(Dc>#o$juLgLH##A=#s-t>afMl3O]b6d8##v[)KV&5>##A:tU)t%4N;-UL7#=pY]#SRc^#c`]=#_W$@B=N]t#DvZ>-8>+2_,fx%u"
"'JY3F?9hE#A%_B#.OsD#SnvC#ggmC#>C6C#kkd(#uK&a#W5$C#@).m/Z&pa#=8a`#Dp9W.@CaD#]ZlS.>tSE#6t:T.Z[ZC#HOp$.Hr/kLCv#]#@qL?#>oKB#I)HV#X4h'#a,i?#PUR@#"
"q[0B#wcP[.JD7@#kkK8/HOHC#3`?iLJhJ@#uLNjL4JOV/8i=>,0Cjx=F&<A+8k+/(G#w%+jEl=Y#sLM'D*vx+^^no.WD5D<1Shi0,*+]k$fx%+c>-/(ZGtJ-&s`N6S-^*#oGX&#Al9'#"
"Jd,r##_sh#VD-(#+26$$A5@i.K-$_#rY?T-dcDE-uh?*081qB#,de@#aS3N0>4-_#AN.d#Fo^%MQD3G#5CaD#LR,lLJ-,]#lEqhLlD`^#j^B^#3gAE#Mf>-0*7OA#++=A#IJwA-ix(t-"
"k@=gLsZ/@#1:F&#*[oZ2gnRH#+_''#/r^^#SHkA#hJco.:CbA#xM#<-7Lx>-j5kn/r4i$#K&4A#u`EJ/9Ye[#5MOgL7HGA#6x9hL%7a?##v3KO-I&VmggB5BU1=.$/41B#TQkl&#]k&#"
"R;8X#Sp*x'v;q'#7AT;-L^Ca3ohn@#[;7[#r>N)#P(U'#`lVW#%=C`.2OR[#ZR4S2(d9B#:+hB#6oKB#v>XA#G#u_.FIjD#;5?l.>U&E#bWS/.-,KkLbS#]#<OI@#^u*A#<5X]#:0qB#"
"/Y_@-GKZe3HkSa#@UQC#q5YY#MadC#?A?_#`l6a.uPtA#Apr'5r#=]#D$Ox#kGI[#Z[[@#?Q/a#f#_B#2>[61T/5'$+@O&#%-k9#Nku8.&5>##xRkA#DE#l$cww%#5(KgL[bv>#rCLV%"
"6-%Pfmj]f11>7A49p'/1=onx4A1OY5EI0;67DV`3uwTY,2/]D,cL5;-gelr-k'MS.o?.5/sWel/wpEM0J-u@Xm;*^#5pj)3,AP##0Mc##4Yu##8f1$#<rC$#@(V$#D4i$#H@%%#LL7%#"
"PXI%#Te[%#nD-(#_8^kL-8wlL[<le<rLm%=vkix=$.JY>(F+;?,_br?0wBS@49$5A8QZlA<j;MB@,s.CDDSfCH]4GDDPi34]r2v6ogVG<fkX59A%Me$h+m-$)cH]Fa0*>G.=L'#a[l(E"
"P7L`EvG(@'+=AVHuv/F%1=auGis=SIOuu8Jn>:PJ+Qg'&'fbP9344m+b0C:)6mTc;T9k_&$=x%Y<7,F%_fl-$w']`X(K/-<]>t+;TE)58>=KI$>w<;$-X/C&l=5/#iAP##^Tr,#RY$0#"
"M,85#_C[8#0b^:#@0?;#UsM<#eG8=#]L5Z,Ss2l,[5Wl,dM&m,lfJm,t(pm,&A>n,.Ycn,6r1o,>4Vo,FL%p,NeIp,V'op,_?=q,gWbq,op0r,w2Ur,)K$s,fDx=-;rOR2mle+Mc9xiL"
"G#JQM&nM7#Bh)'MpeB(MMX[;#2s:*M$vAUdR_.iLOVOnMGv$]k9DH%bR0g?KtIkxutsY<-ivAgL/KV(v9WwRn@WYUlNLGUlJ@5UlF4#UlB(gTl>rSTl:fATl6Y/Tl2MsSl.AaSl*5NSl"
"&)<Slxr)SltfmRlpYZRllMHRlhA6Rl,B97#BBo2#((`1#okWrL_?6)MQU?>#Y9OA#J,>>#NYlS.6,<D#]M#<-l36%coU/+Me8T;-VTFAJgD&Y%=Zt+;jrTc;c,&29Q6do7UT`l8aDXf:"
"_,]i9.8G]F`K]rHbe1DEO4L`E)vEP82Ht87a5C/#jSq/#o`-0#Hpr4#K)[0#^P)##S][6#1jmx[cNPf?c3X(N=qME,;[u##gfFT1_d./1h/mjD$MglA*x0^#X7;X-O>d'&XWp?-BC3p."
"7lTA.G<tI-;=`KOheVJDHG+N$Vu8gLT6hKO'VRV[8<qlD%gk&>8F4g7*Z`C&_=r`?G%kB-uH*nL'BL[-v8Y'8v-)(8CNjt(I@K+Mw56Z$lTSV[J?WC?S/YALt34g7RSHY-Z]MZ[=cQ]["
"ekGY-(;`'/&dELYLtR5K4>D_]+Gu^]LvsjLEh:dM=[sC?5m%V[<QeOOwZna=q7[V[)PDl+%Lc%M;>Mt-+[enPU7-###YjV7@SF2_$TNjLnL.p.M1#/LpeWnL;5:Z-,.'=(X;_;.:rDi-"
"jZ-_JtCW0;AglmLs=rY?_$Y6#_7?f_R?3L#w5qB#d'x+Ms@rB#F/#H2iuJ*#+e0'#;####l%E7#1RR6/rEX&#%(_HMkFrCaO.QA>J61X:PS:;$-)'kLUdG59qO9'##Vs)#O)IqL.omuu"
"%,>>#l/%[#mF,h19IIwudp`s#prn%#omtOMX&rP/$/]6#*3pL_9`t>$Q?H=(1/*=((`^%O:$;-MAdNBO<0M-M<0M-MPBro_Iwno%,7pK2@N:u$v;Tv-GI;8.]::8.Fc7C#0?=c4*^B.*"
"BlnU/reIm&EXa5&L&HPh+L,7&G?5n&VfvM'NaUK(Aa?o=#h9Y$D#Xp[CBV?#R-/[#o3Ee-@TpLT>[<wT8RR:v9UR9`7:*20kZ6JCUw6j_Oi4;-e15j_'pq%47Pk2_beD0_C_[&McKn20"
"9Cn._Ci`3_oJk-$`ts'Mv_i=-l_i=-)M#<-5@;=-tkl-Mcj@iLWQAqfP72Q'';SBfsUFb3sJ))3pr/f)Z6h_,gU'f)Y=S_#CvMF3x1f]4`b%T%h,hf1hiic)PH=&=`v;h(AE*M,A3p@#"
"TQf[#<aos7uCSP'R+2X'j*&@#&F[iL0'2hL1(35Csqh&$5u529w<%=-9'Cj*`ilY#x[@Cui/hq%e(@-)wF*a&(D.tHf2-7&w+,##Jv=Y##`N1#V4RbO-,2-2XaTP&llGs-/w)JMtaYgL"
"?nU`jAEl34:Gw<(#3=1)sF#v,,R(f)jAqB#5VTN($F.J3Vn#w-Jfte#;.Q<M3hBk5;ed`*70q4TlcSSS'2IW-N;vLKfLH>#eMR-)NWcD#usQ[-VmLO'bG7-)>5YY#&E'_S]C>DNR*]?T"
"XMZ@Tg_e?Tikw?Ttiou,h?Dv$'JiK)e)Y6#-^Z6#8>pV-90K_&=?&9&bV95&Rh^Z-c<SC#1V'p7Yv;a,D5,x6JUFb3*6qB#n)Ki#`u1c+Fn*.)w0cc3$5^;Mk)_'+H_0sL-TYJ,W$2hL"
"Brjf:W^uf(9Pbx+ho.P'x.4b*6Ke_s':*/1Yfv$v@Fqf#x:F&#F1SG8&SV8&>/(<-8KPO-Gsf$MpPPO-0Im=M2)2hLMZ3r7`a7p&67cA#HcVd*i4=1)-7d]%:(7E59&#d$a<?<.Bc>h("
"vnK%ON'JIK@g7B#JnKO'.@q^#Nv3.)80:tm?K#F[TU_E[jd;DNxx2>5-qd>$j<d>$v;Op&=s;qMj`IG)Vj.$$`3l6#_q,aNqCi-MVQ4I)@ZR/&-jfYHd*4;eGt+#ZS:t+.o%FcM(k9U'"
"=kIVnYL#B#`R5kO>DX`N>v#w7Xd$u%`=Y3Ov:GkOGgt2MdAZ/(<V(ej/wd50v9cn*$OT`3<))d*$]X&#H=L<hQ^_3_N`g_25XJw#)in$$,`m8.b6FA#$T)]6T*gm0VE/[#&4`[,>J))3"
"LMjD#u-3d3vD3$5i?R`E=e#-3l'YJ2VfG>#KVU_,k[oU/NJSZ2J`+M(bEKQ&-qW6/u78l'hsCq5jr@@#ba-@-Sm2&+J)QA#V%Y70qhHX-v4h0Mi=[W.2=i@,0O5_'u/6R&t;taE2qok("
"1,>>#TIu[t4F:AOn[j.L>r$78L.HK)''d6#&Da=(a*&39s@%a+X=&a+HLi;-/`&k$xq:;-r>MJ(>fW>--urp.eha6#F#<R*EDdG*7KkM(@`w],mV6n/_R(f)WOFb3[Y,<-h=Lp$4*QO'"
"O12Q':O&c%na0^u$IMX-QRVe$F/YY#$Y>W-^1Ze$)]bgL)Z-##[go=#,`N1#2)l`OQ'g+Mt&g+MeHHf_1`uu#,7pK2_8d8/vCPG-B;C%/]#MO'L5m-?rFX-)QUQ31L^Ifh'S58.iL*m="
")([m8XQf#.F%@#'mUGv$'bwP'7d,g)(1?v$Cln&,uW>>,*MB,.vpvIM6$1P%vZ'u$,Jem0p0o'0u3YD#hK]P/U3Y[$P[TfLW9NO'4%m`#TmO4MI0LU'k*&@#aiNN0%^QO'(gVu5Bq%m8"
"Efis%_-&@#[ulh1$),##JdxX#jqj1#_kNcOR[%.2aS.,)$EB<-F1v<-SR5hL>mv##r+?-.hHFgL0h^nfdI`s-P_VR8&V.J34tFA#KjE.3fgGj'3>9^%Ewjp%800-8mJ+q]RHBq%b,Ajg"
"+3HV%GTa8&$QHA1S&OI)UnuI&]A)8&QBF_0ep4Y#kka1#LR*cOOk<$%pV#39lRxB/F:tQWb_35&hFBwTmuKp7&gkA#wV]F-%Axn$BtIeQmEx3M%QWIh(,`B5to4Yu&Jpr-#)Q.HR<Jq7"
"kVd?$ubi._.)`3_qPk-$4oD0_)fZ&M*ocW-B#%:);WkM(;WkM(Go[A,BJ;H*c<SC#O7#L%2v+G4S)ZA#8mP,*O6=7/v;Tv-5^;:MGfbP&RKT6&9Ing(TjlcN.11B#>'hu#6_-_#<:06&"
"52A%(wM9pp3ku_N.),##Vv=Y#%sj1#'wfoOHOi-2_AMJ(.*op7glpAZBbOs%9qV8&j`X8&wc$N0)####;r+@5%g)+N_ig._Xc6*n4<k-$fr&tq$$,F%$Y'tq+QC_&+QC_&GPD_&3jC_&"
"3jC_&9>0F_q+D_&WsJF.#ag_2Ma(T/%qKB#m,lp.abL+*cjGaPS<_xXBYcY#Tg?38an.9&=+2?#M;e^4;rlY#_$6ru*m*.)9#^U'KeAm&nS*Mg+iFAu%ZLhLGa6##`,PY#Nla1#SG$n9"
"/>4m'VF;m''I]g:KxG,*)m9m'HwkA,,btR-&#l4%/>8U)Yh9U)<*cNb7Ps9)SWK6aCB(u$HO=j1JUFb3#C.J3p&PA#mUsD#Qtc8/FaAZnO/xJ3F$oB6D&e[-C(kJ2P)`u.'uOcMV)5',"
"_5X.)c7p'+6(&W8E9vd3qD7I,D6p@#22m01pc)`#>lSfLWe-29FhkZgEY9wgV>JMBo5DD*<'<J:I)#q7baBj()b66U&:#gL>#DP->/</%_>W8/1XX30$vjh2Hq.[#_`Pvemi/J37ZRD*"
"GB:a#[p-w[*5.k*EObc<fMR-),okJ,A3p@#O9>j0bG_E<<PE,3'[-PfY4.S';XT-)3.tJ29F#h(@='^'YC2ZuofLh.06[S#-]Lh.,>N)#_/9d)]PLp.33pL_6RY3`lGm6#>WajL)8Z6#"
"H8rl-?GH3k7jB3kBgk-$Wi4?$[v@;$e73v6KDhJ)A;QK)/4rr-)7Zi9uaj/2E7w%4c8BK2>B(u$'J6qp)f@C#.Kgf186M2%+TID*Ll_v#svaG'<eXD#3#>c4+g^I*Ai*1(SI3p%Lb[W$"
"%lB:@Pt:j(Po$%-17nk(D$]c;Y(qL&f<j4)B*,3'ho;2KOT4U.Xgpm&dBw>#a7rC,28jc3h.dY#1Tu01E>lR(%e+h'``q<1hoY##b:a%vV,Ki#u.4&#wJGU;XoV3rF5D-2Zs52'UT#N0"
"V3=&#b]m6#AFIg:'ZB_,vaYY,b'Y6#8^Z6#HYlS.)ia6#6k[U.oTY6#2M#<-@Rrt-GbD$M,Sa$#Er+@5AYXP9rAT;.UZ,30+L0#$P.i?#x:Te=%J9f3xZ'B#n+^m&PZ>7&gdNO'eGDO'"
"&p=r/bDkg3=EF_$)4Cb*4(u6'[QL,)047xkfMMO'>hpH)/lE/2Z<fx6V3_8ef``4'L;b.)[3n0#+@t(3LP`iK;j6>5XTbN9oT0j_nHel/<6X)45P2_Z6FRaj9K#^5OLR/)Buq;-5f'S-"
"ne'S-sWS-;VYbA#Sn5s.Sn5s.RBg20;Dkh2*ZVO'UQ]NM1/Zt$d%NT/,u587YlXv5s`J>,X&l]#+L0#$/qW7/qBq^'<#cWQxI]1(s/:-*hmt;$ue6<Mrvt]'6tbU%0:%],t-feiBQL_,"
"I01,)[j,7&a7Jw>h>HR&J=g+6RJ/:)0>p'+ds)b*R?lf(fZq&$emJwuZ7W`#cbpe;%d%w[/w(hLe5W/:_snP'R;Hq.oNv)4mm/I$:)'J3X<f?TcRFO'pAP4M+Ou(NqrC0)mH5h(o'WM0"
"c,P/)uTr21hJYiTgR9s./n3q72>F2_^Z1.96:'$-bXD>#Rddl8(^K/)a=Rp.G]R_#J7fBJ-;;.;g,hHOG(RiTC3md3?KQJ20`4GM:-sg(Z.dV'>[(hL[LT[#hc$],0L/@#A*dd3)tae'"
"TRMZupD_i:3ZkA#rK,hLRZ-##9,PY#Hma1#MO(^8S^V3;cR_5%5t'-4_E*O9HFZg)''d6#'g;F&Cf*=([T>ND[-w##P0uJ(%Weh2Z?c[$B:)`+A]DD36'PA#2kjXA@/ga+D[@w#K*4=$"
"a08r9dl2[$Jk&Q&B/no@P@m>#I'5J(,R+n&PA%P'D2:X$?72$#/iLvu#@fl#,Z/P:G.9U'EIHB?wZwKs;o`u$6Wps-e?i%OYp@C#]::8.@G.$$7o%H*O1n'&5cM,*H@hGM1<P.)lRx_#"
"(jeW$]A,n&xNF(0lYE,)Nwnw#b<gc0.1wF8J@>3'[1ffL>0YW8$#d'&meeW$v)Nf%w_9DN4sL0c;3C0c7RmI&JFiZ#Ab0Y$H09M'0GN/MFm92'G-9[*?8.$p4;p'`^'$iLa=M#vCFqf#"
"$`''#,b/%)h=Ro8PQ[0uWw1gLeeYh%?6a3tHc7C#Fn[0u)avGXoLND#V.gm&K^LK(J)4b$KLwS%glvPJg:,3'PmlR&V`xJ(F$^m&3v592TU;Z#JI(=kA1.s$aT1,)v_EX(F%O=(BkM<#"
"$,GuuWr4Y#Urj1#uoVfOUUr-2`Jif(uu><-D1v<-mqLq728-k_@URS%XcRh(n3l6#Yt@+%7^,F%Ov'T.UsB:%V6FG%T=Yd3KjE.3.iSfLa`?C#3C>j^fMR-)PApm&E02?YGiuN'u%X^+"
"`XgquN=te)F@.@#t>)8&#@B1po0PgLwuAJ1N0x5Ku(9;-K6n:'.7H&%*/gE[Bq_Z%TM$'=2HV)+P0NQL-Y1`jx?cof-0I#$C8F$$U5q%#,5>##45T;-D5T;-T5T;-e5T;-u5T;-/6T;-"
"?6T;-O6T;-`6T;-p6T;-*7T;-?e1p.)GS^$.UNb%YXr-$a0V8_A/U*<[)+Q'Q7DnEd(vu#82e4_wn:C?=pIiLQ;4/Mw0PI;4Bg;.ue+oha`+c4.uB%%]HL,3km'u$:J&2B[v>R&wL$W$"
"Cq/6&OaEZkYm5R&U8;0(qmCfq;0vN'>]U;$S*b9%JA1<fk),T%iZ;<#3RJM'?EbrHd-42'UR3]b,%+7/xx38_D0t'MCbd##5/B;-GAg;-U5T;-f5T;-v5T;-06T;-@6T;-P6T;-a6T;-"
"q6T;-+7T;-JkJGM>*='M`2$)MXFoo@B)QM'#Vbo7I,+Q'uI@['Dmk-$TGl-$exl-$uRm-$/.n-$?_n-$O9o-$`jo-$pDp-$*vp-$:Pq-$AXBMh_kHYm$oK^#>r%v#wLqP'uNxP/l^TP&"
"C%gb4m?>01(@*Q/Xie:/:'PA#P'ug$-E:B#5=D@,]QL&PS2,J_=k_^u<P9U@[:H>#5[R=-vIF&#$uTB#22X@t@2./L7SgYH&^hq.xx38_.Jq,+&@,gL.hZ##PGY##U5T;-f5T;-v5T;-"
"06T;-@6T;-P6T;-a6T;-q6T;-+7T;-JkJGMh)='M`2$)M?>iP-Ul[Q-?CAZ$p3GJ(,7pK2mnYd3x9%J3,ATbG8+O8&3QLN'Up_g(_-v3BIUi?#%#7iLo&16&MVFb'ha3$31,#$#HlKS."
".S+/C,Hmu5(@_W&mGm6#;eHiL@*>$M%K):8Cv'B#fGui;>#rP0P<q`$-pZw'RF(;0?LdofuId;-9IVX-CJ#1a,`7C#lTU&$1.^Z$T=S_#RcZp.c9FA#-Aew'dCI8%m&l]#pCM2MlO.25"
">'NA+^9@G*SY@g1-aV-)?-@l+]e?c*$#>JMp)6/(M=>w54A*T%c*6J,7H6R&Qa+h:h>HR&eD7h(-YeM1:E;,3t8ig(.8.6MbF$##Q/$$vS:`r;oW?j168RU9lY1#$1I%n(lI['6E#[[-"
"ikZt-=9h/+k)++N4@tg;jBR2LGQ0,;o3XB,E9p@#2r3j1*/x^$0aLf;KQhB#2DT=u@@RVHNo;X86#?p%B(PV6Uwvu#o84O%:H>m/vts6#0xw%#AIQa$[?$)*RLl2_WFD0_Jt[&M55`3_"
"AYC0_H_E_&9Ri0M0eA%#9A>d$h=@8%GkRP/:jE.3p&PA#pu`,2D3Tv-YekD#L,k.3%Ue+4pmj;-Z0of$iHuD#)VPh<pH;-*9'`C$ed3.3-hhG)Swnw#1NO:.v=Al'j?i@,UKpm&o_1[,"
"]6pi'N>l3+:#cv#eY*I)KvI/3tXfU/k%Bf)a%'H6MW@;61Z`4'uOAT&k?)kBf8HR&Hnw<$Lb<5&J'3n8hAQR&xv>3'a,k$6Y[$=-,:F.+q$rA5K1HP/tmxx=g5[]+J-Po&DHXb+,0`/:"
"U0;d*;&ikDB7x.2iNOgL0(V`j^_*2(g.smf:qZw'AV[w'WE0>A(Q()3nl5<-OQS_%FNCD3oe+D#SUZa$D43+%_Qb[#jP2?uFN7q.P5MO'ool'&;5Dh'$Pr&0Qv=Y#cSh/#$&7;+8=IP/"
"pL#j_@MhAZ$.gfL]A7ajh.w%4j2T2(vXm;-)OCX%Z9=8%'6a;%Ttr;-JfG<-X:#-MhFQdj$,J^=l%wY6r@Nj$LR^fLj7U%6NSq8.m&l]#MIk^,UTN]F>rV`PRWpfMOlv5/c;td*m(JFM"
"FA:F((DO**X:)9J`JE,)?n;R8So;l(ZYN`beGDO'HQv?(oDrg(A#H&#uX9^#`qB9rf#E9rk9[i9=)v_ZkAm6#4K08%Y%KV6DUU50TW*3182e4_@2MBoXHiU#whJa>JEu`4Z43H*Zt$##"
"VrdG*.23B,PQ-58r;Wp/-v/20.7O&dV0S8%kwfv$UJf_>o8Zl1#.,Q'$j?A4Y82#$+uS&$=Wm$$_.i?#AHZ)3c_S7/l]q_,Gxg'+g#BA,u,J.MUANp'uMvN'.c#C+mw'G3Z_'^'hwr?#"
"t:JP'5^UB#ho.P'^Q_$-SvMA+H#HA#N2*x-B%kJ2x]R-)d2;O'1=u,Mns-_'VO_w,K?S>MBnvjBf8HR&dH:u?)2b'#80CG)pIj%Xmx48.XBt=-UA37'']>>,;(vu#s`IG)dA,F%T:SC#"
"6m;*eBkN/:etQ#6uH+g;P09Z-Z3rhLN3c%X)G_r$KL[s$lO+gL&jMI..oO:m>KND#(gLv#;JlY#5aP>u[N.w#R'j?#o/vN'ml>I$Vl?8%75###L.-5/N<F6Kv*af1sH#5A_%J,ke8To8"
"xoH#6Al;tUcdFp.XUNf_<-9pgH0/@@2AP(H^s,/(CY'Wf&5voL[/pqf?^P12U2SnLgH<mL/Yk22ckGQ'u3vr-ZI(*4%[*G4Jc7C#)<>x69tTfLW12#$jg'u$^4^F*]wEx#JGv+<4N:`-"
",[h/)YjWj1)@5Q'c3w6%;/#h#Xcub+$(Y70T?UG)=Y1Z#4W#S/h5YF+8_,3'JW-)*<S(Z#eXaxbu@S1(u@SP'=xGG*x?i@,.nqv%Nj;R'h$s?#xI]P'[dH[-?iE/2M*tr.Ai:Z#($iY?"
"MivK)a;@h(tA`D-7i+gLMNl>-%g>5&PdF-;hAQR&29@.MkS-7&bxCjrVwD?#IkxlB';sG2%/5##Fj+Y#UM_/#BoA*#;e$^&06$0167>##,f%j0p9E)#Q.r5/Pf_0(ExK2:%DTZ-[=PV-"
"%pk-$UJl-$f%m-$vUm-$01n-$@bn-$P<o-$amo-$qGp-$+#q-$;Sq-$IIRs-X'#)M[4oo@jK<#.$]aS9,5P)4fA8UDvC-5/`J(<-qKt7/uF-.2GC2G<1Is;7i4=1)DI+T%))4I)]1Us-"
"ZI(*4HjE.3l5MG)SwC.3LMjD#,VEj1)[Wu77U2U28H7l1tFI?$'^.EPQC%D,c>'e*S/vN'j^#&'?sA*+N#HG*w9`@,@,hR/[=p'+i#`U6,*NA+Fj,]u3-e[-jhHX-Dg-EP1J&P'D:sR8"
"Ah-(#B/8>,Ou3GMQCRP&^QCoe(Z1u-Kk<=?k*h#$JKto%B9f;-Yu2:/Sd0'#;]>lLfs&nLv(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M;x_%M&tM'%5`@rm1Lq^#<4l6#)GY##ukMs-"
"g?hhLOVOjLTb6lLemsmLuxYoL/.AqL?9(sLODetL`OKvLpZ2xL*go#M:rU%MoTTC-NU,<-Z@pV-a6RWo)2Xmfe6+gL$X9nfk(:]?7=Pd3*^B.*lqwm/Fp9+*5l'f)E]#<-Xl><-31T,M"
"INhx=T?^4o<ZTP&9&jFr,*_a<Cd8FGA(].2g4'&+^/Gs-x-LhLR+>$MT0gfLZ5Z6#kj*J-d6c[$*Q@D*j6IP/U5u`4fq'm9vV0#?0=9/D@#B;IP_JGNaDSSSq*]`X+gel^;Lnxce=kKl"
"OXNb%Z[r-$Ud7H#?n:$#:0HD-(*HD-_m2Q.a]Z6#2[mv$F^R##h#8>-bRP8.4dTd+cfD&,xUW&,4@x<(WPl-$h+m-$x[m-$27n-$Bhn-$RBo-$cso-$sMp-$-)q-$=Yq-$kd7p_8U9',"
"RPRh(&3=1)w&hj0]w.[#3=Y)4tHhx$?4j5/kb';,@Te.McK<Z#*=_/)rcT<7OQ:g()V9F*ANDUM4-FB,<UU8*<vHV%@YvW.d+]h(Vr###>ke%#*BAX#SM7%#@O)##6t<$MK*^fL)gdh2"
"Xnm/2cu+G4GC4T%tCr?#6ld]kRKKq%n52?#(-(n&F.;?#%LEA&QW06&5iTU%sTOQ&qlcvGn:o%#7k+/(;=/VIuEWS%LAe#-=V1PSY;rg1J3pL_N*SS%v=o6#kq0hL$:kHA/<ki_q?`P%"
"H^;Th0)>$M8Fw68Au^>$>nq>$3IXt(TGl-$exl-$uRm-$/.n-$?_n-$O9o-$`jo-$pDp-$*vp-$:Pq-$bLeL*NLH##g<iT.;=]6#kqC5%4T,F%rsN^2S]J,3ig'u$Yv<FN,d_`3oEAN9"
"*qcG*?wt20&#958B0<d*p'aF%ZEZL(QonK(Q)N)':[:(&Y2KW-Ki@m0BD#W-]+8w[)fD[GYg1,):C.q.$,>>#a0?6sTIV]Fb]us--=mq7Mdaj16HSP/?*0T(i$Z,*atr?#GO=j1UjFZG"
"h+06&.?]vPD9Yj'$]Gn&mGDS&A`3I$;=$ENWDrk'G8pE@Qrf=YWuLulbqRP&8O.]kC[Au-ur`g;#cJs7s:4gLnQrhLE]XjLUh?lLfs&nLv(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M"
"Q=Wf8t6[ih`tdum[WHH+B8xiLXHm6#X8#LMOF?##.iu8.D<O(jdB&8oW310*Tkv##@lP<-G5T;-W5T;-h5T;-x5T;-26T;-B6T;-R6T;-c6T;-s6T;--7T;-A[lS.HIo6#iT=a<?Go.*"
"q52/(C%gb4pxJ+**c7C#e2k1)=v-W?7ANZ6qL>c4mdp$7aH/[#FIt9)aoQt1s5F@,[[YG*M_p?-H4=K3fMR-)ELn8%''Zg1h>HR&ji0j2hN`MBOi?F<irJe$pESx63/W-)Sv)d&;Q(v>"
"'0(<-j/;0(MCY<6+W(v#H.%##4UJM'KPiiK:btx4q$FV?jqh-'a/Jt?+/Eacb*1U@P*N@$v=o6#7Vs)#`PZ6#RTY6#-Dpk$cOBD*L5KP/U5u`4fq'm9vV0#?0=9/D@#B;IP_JGNaDSSS"
"q*]`X+gel^;Lnxc0-+6'r[R##OU,<-bwhP/>*Jf_91gr60?3j_5'nr-)VZs-$@G)MppK-%Glx%+P'M50WGUA5h-_M:xigY?2OpfDB5#sIRq+)OcV45Ts<=AY-#FM_=_NYd9iHwBgUrr$"
",IJ,*DFP8/T,YD4ehbP9uMk]>/4tiC?p&vHOU/,N`;88Spw@DX*^IP^:CR]cuxj<UNUNb%YXr-$`0V8_+IOBosbpD_ur@L2)*'o8uEv;%nQG&>>E_s-Je75/.u587E+]]4Y>%&4$F.J3"
"5Hrc)M@i?#,Mx_4A.ikL0NB17_T^:/x)Qv$jIkI)U_=V/C7.[#x@^Y,k(TF4WXIT%LOKu%G6KQ&=':cF?3a[,0L^f+gb,K)47-0)3AV?#PS((,%BjX.]-bP&XJPkPGRrZu`ujJ%K:d%P"
"nw`V$TfqnVJ9MK(3Ql>>a,$R&^Yo^+*FZt&`Hc/(wY[N'Spb;1Y0[O(f,mR&,,Lj(TQ[g):Ug,3:D7Z$#2P-lP,US.i@:d)Dx`T/YFV?#-U`R8HS1U%+2NS&]PRW$':*/1Aww%#gIs`#"
"NI5+#B`&v$eQto%5luu#U)k--(0,F%Vd+W-EONF%G8-F%ouC0_jjI9KO1CkLxqJfLd*E.Nhs]/N7m(1N0oetfpj<RU0)%E3%dLm/]::8.Fc7C#g$]2&Nb#w->;gF42A3S9(7Gb%['UfL"
"v.%`,h#CD3p990111x;%a_c_+OAlf*CXEj0tJj]+OEC,)&=QaNE;D)+aTDp/C>q*%>w&w#T(2v#RT=x#G`;`&x#+cN&KUs+ROa7S#Nx)=N1)v#uWxJNB-Ju-b-7.Mg_vHN@&pNSe$###"
"Ka9B#c`Gj#usC$#3d)5_mTj;-q+jx$_n]f+(F5gL4iTnf,p9D<_PM)+tVqk$h8g_2ZQA.*n&<E3p,B.*jm'u$wAOZ6c$vRKO?9q%^oH^5Gn82'34%6/(-pT%:4SZPUX$##u^cF#IAvr#"
"7M7%#'.=J-B0D2%[.Ds--$mm8)j'B#g6+gLqG=,%bQ$)*OhP8/T,YD4ehbP9uMk]>/4tiC?p&vHOU/,N`;88Spw@DX*^IP^:CR]cBa'<-NU,<-]5O7%tcGq;29V3;3[tD#XA2T%if)T/"
"5n.i)Hun$$Ig8F*$SFi('oK97E%u.)n$&[#;.d(+d^F]#EVEo&A(AQ0f=:n0o0*s$xWc`*4m5Ra@fQ/)7M[?^]I/2',7pK2Fu0N(aqqm03>%&4<9Po97ajZm5:V-)BwPZ(Xtn8%-Usv5"
"SMVx4eMR-)Hq@w#Lbiv#eD7h(XV#]kx+(J_JaG)+OW,d*,VCp7.axQE[)8nN5DF#$,(Jm/6'PA#dZ'u$jD?I%r6^M'CcvmSF3pm&cIP<Mx#Sg#r3#REc-cJ(2riL'4+AbI:nPfLf`.iL"
"v7`^#^5i$#18U%)(8J^=1isJ2FdmG,)s:u$:7rV$F$+=$[A>3'Ng,V%3hZ_#=-pQ&0[L'+>L.@u?).iLA(2hLpHn+WFB`-2pWdTr#3FK2f=8C#GQ>8LKt?a.kknW$k5gV%pN>R&G)N&+"
"x9UV&ev+F%m4lf&DCZt0b?[Y#0qQcD50Io[xFNI;-D).4@aYX;banF46O$g)u25&#R8q'#[L`,#NtC$#cb.+17=G##[e%j0;?=8%$Fjs-n?hhLOVOjLTb6lLemsmLuxYoL/.AqL?9(sL"
"ODetL`OKvLpZ2xL*go#M:rU%MX<=,%2P%Vm*x0^#+*1u:;rWj1>VQg'4A^;-GIo3%;`='#q-#'v2iPj#WN/q&RSW]+rPNP8^f$q.j3pL_)l52aq8^3M(#rHO*pY4M1YnIOSft/M*Hnv?"
"j`aJ2]saJ2xpJs-93xO9&vCT/;bTh(37:pMbDZ6#6UY6#;QH_Q()Fmf@tC$#Gobi_xd-(&%[NP/pQo;-?r:T.sCa6#7M#<-YM#<-V5T;-.fG<-.87Q/a1Nf_dM.;68d^;@bA>F%r^,/("
"22=2_1FCr0ApIiL()doLWR;523%###5B$2Nevls-e71pLGklgLgen*N7SBwLDW%iLghr=-[4GG-LY#<-j6(V%C`P^2wei?#a9OA#F1x;%[ugt6Ac``3U;%E3B9Do0Um'u$-HTs-AV%N:"
"dt/aud/@l1bloF4&,]]4-'MetY0jeNaO_5%,qi[,M]8a'Ua:O'a>0^+w.3k'Vw&J+M2***95d6'EC`?#16Ke$dOT-)avvT0,2$p.5';hL8wwT.-n[5'^'uU%=aR-3*@:B+h2jB+?lbru"
"Hfe1(U0Hr%rs@@Wk#G6/QHT6&>s^e$*'Z3'e/[?5n>mX.eF(W-JfiD+RdhA4eMR-);vMQ1FYkr-E3a._cY`c)3'#b9b$&X%-Suu#rV.,)eD,F%Y3l6#'nwK:eBb2(Gdg$'G@Rs-$R-iL"
"PikjLWtQlLh)9nLx4voL2@]qLBKCsLRV*uLcbgvLsmMxL-#5$M=.r%M_$(q@xXPL<rY)n#*=_/)r`T<76'k)<F9C'uO,,B,'#I.2vcEi(BW@iL]I-a0+hj1#G2c6#PVl-NcrS%#@Zs^%"
":anD_o>_3_YLD0_I33gL_eir?0^BN(xk$],vHH&%HV7C#E+tD##E1aYxr(u$M_UcDk(wR&Sn.[#>gA[6=B`M2/^?T.]dpQ&C;e]4+PuX-IK>R&Ft@@#Jtx_#,B/W&Fei?#IRR8%<4=$1"
":+6VQT8l(aekLS.rvn8&VHOwT``K-Qn'=wTWPKkLex(hLN*'j0wUXb3+h2gL8-Zd.Z_i?#Z+lo$?0'_Ss:(KM3&3Z#<CR>#c@U'+v3LW.%jkJ*$Wuw0Fn7@#GU&Cug:s;&cX)UM6eA%#"
"U7b(N#wSiT6AFP8%dkQL/rsj$O<Z9MC.>wRC'Wpfa#,N([^k<UJRPn*F#?u'wpBP:Z5vG*HQV_$ZL$Q%R$)T/b?rTT_)iX%u)'<-[f0j%,grk'^A1C&-88?5bLes$h0a20RH53'h$96&"
"b*j20,[P3'ibljLBQ>h1n696&l&1Q&CXET%*5xfLOTT30>,>C/;Z6C/7(P,M.:a=-%(IM0TGH$vFc<T#D.0/D:+k#J_Am6#Qr5@&n`i^og%B9r/]OT#QJUs-iNOgL+rJfLC'2hL8+$of"
"JTO]FwFM#n`U$lLX`^@#G&sH):I5V)3TA_Oj``O'31vN'=BI/PP]qO+wvEb.Danp/*cZ(aU30eO:&1='edTP&CK(,)6ZtP8g9)I?Wp(;?_:u`4ts==-ZF0`(fcdO0xj^m&f(Rm/>),##"
"(_$o#Z7`P-A[,h%:2Hp.%o'c3OS$(dK(e8/6r.[#Em*.)Pibb'k'SF%8ST603V*#5+0+20p`7WSf)p%##vCp.NGY##EU?hk'iA1(.WScj,oD]FD7?>#+T,<-oe,<-Glcs-c3OcM?O1DN"
"R+BgLhU>hLSLt'MLoWB-;xS,M;LObjx(.<.ULD8.kmqc)7w:<.UD.&4VXq=S,FIx6TOr_,Kr_u7wI]P'wF]1(b:rC,dA'e*3SV8(^Ks?#a:.`,3K-vZ4Mc%nJetf25Oet-hetx'Z####"
"gFs%vJ<3h#S)V$#uP?(#dnl+#6+6a&+SZb+h;gp9U>SD=)QSD=O5vM05Xt&#b]m6#vWQIM#>`'#bfYs-dxefLZ5Z6#uwY-0%Da6#.^Z6#RfG<-Y=%q.<JHf_7c.<]J1$##vJ,W-?_<bH"
"kJG&#I3s+4DsMG)v>Tv-;lZx6)@_Y,uV_)37iN_ZlGGA#ZCQLMB[Vs-qhpr6m+0J3LY:=QGN5g1^Mbn8`uKd2<4i?#n_D;$a#_7/ZAcY#`Zb:.nI[5)mx/J))LM`+OJu`2/O&r.]J`=."
"v$Zg)27Uk+L<.IMFxq[k]XA>cNe%<$5.r5/GhI<$r19jLl'FdESaK/1d1Ti(*GD`+vi]_+c7Fd33S,lLB(SC##3kU.S@FeD?P'<0GY(A5Q*/[#5]l?Is>g%#?&Io#IpA*#>@C]&rxQ*E"
"u@vV%Yl%6/H3pL_#__%Oj?<2M'@8v7dZ*n0W'wu#2*gi0uDm[$.B,F%780F_kV,F%<m,F%'-,F%F+3KANrmY6]DYD4`/'g2&a_p._&KM0A9Mb7Kvt;-bw7>-$pt0MTIhkLCWT40Avjh2"
"1rU%6@`wQ:8nT&$n0+9.s`J>,-.n;-.X'H%R6ng)Pa^I;eclS/vKBE+lb7Z7s>Ke$3'Go/F(*A&oUFU)b/le$ho^gLK]+&+ub0/)po1@GNkuM(eMR-)ho.P'.eGb%8I1B#0eL^#JmW%$"
",`x8)%/5##V,>>#uaH(#cD-(#ppb[&U&Yb+6W;r7L*0Z-nQ$qr*t/20*.9k$)%$qr7:hU#E@7[>u<kM(S*2p/IabD4fC/Q/@f)T/e-l%$D^n5AN>P^$GaT40P1*t$MRw8%)'TF%$lc.u"
";T>,2^.KB+E%`e$iu][meJR-)2Pto7AJaEeTR@I$qU(n06:$)E9t6;-K+SV_)q[D*@@N9B)c7C#KjE.3B`#OX#$pI$:bBm/%NlZ#MBKq%2iYs-/d]GNn$_q'(pOYmJ>o^#Zw1B+rtkGM"
"MIZY#C6aQjTf/,MoJ6A44g6q.M3pL_3FN3rD/x.M^/tr70IE/2G`2T.`TY6##JP4%U9iU#JZI%#lH1X%lbX5_kJYA#0:a/:C=u`4=,kM1Uo(O'*qop%O?9q%ZFXe$Wv(?>NiHQKdDDO'"
",cJgL^d7iLt/@tL?V@E+O[c]m6Fu3+KnrGM_U$##Q[`i07=$)EChNP&Q7R&5qWYvR@k>1%I43<-2hQT-6*36.V4+gL,Jm6#53EJMMtJfLJRxs?K4=2_24]w'S71F_2Y-F%`WiU#TAO&#"
"XPGK=0uwK><;Sh(*dec)p,mi'WcwY-KY?<-b(H)%[,B.*src8.qwb%$VJ5Q86?uA#4)^clCQ-5N,:'NKdDDO']E-u'`%t9@,Kt^,lj4]uN-vN'=^kW'Q),##*WZC##YE1##eU-IfE<@$"
"v=o6#[_R%#b]m6#l+;MM]?a$#J`Ys-jTXgLLtU`js<LS7sl[;._1jV7Y^nM1Og;p$^Q32aqfc&#1Nb&#E0Os-7R-iLPikjLWtQlLh)9nLx4voL2@]qLBKCsLRV*uLcbgvLsmMxL-#5$M"
"=.r%M`'(q@,;3jL6xEbj,,V29t])w$RJ))3T/)(=&$K,3J_<r$owkj1Lpj39%*QGEuiAZ5YClv,.fo1(9r$?&J>MZ>04LJ)@o3%$dt629`3C^#<0I?96cfx$uj<Y-BOX`#RQPN'-+)%,"
"b:p?5u8t@9ed-(#*QCV?1jk+`m#dc)qdai0IBLr.Le[%#PspspR;qcj,'ipf_2,/1D`5o$8_;/#P%LKMM8Z6#+M#<-h8hW.nCa6#g2]jLPVOjLV*(02iK@K)tfguG9'F^,Oj1,))Ao/1"
":=tD#DPjD#l_d+`aKd8&s.jt.rmTYXEucV'^nI<$Vn:B#3Zpd6_Qm8&iP2,)6Bg[ubY#7$vCSP'05*-)Jbw&,Yx8[%Th$##A)78#h^(?#1V'B#TNF)EBqo^Hm/x)EmTa=(gZE&O,MV<H"
":53=(0F518W@0:#U9>)MU=oo@2iRS%;,)d*FX1p/V>:&5g$C2:w`K>?1FTJDA,^VIQhfcNbMooSr3x%Y,p*2_<U3>d9d'7'>r%v#MDW]+:DEl_s12(&$%VV-Z5l6#hV40&t2%)*OhP8/"
"T,YD4ehbP9uMk]>/4tiC?p&vHOU/,N`;88Spw@DX*^IP^:CR]cw#<:gdpS%#cnP<-2Mf'.iK$iL/EYo.G0xr$?fqh$U7wAMbkg40E/Kf3[j:9/Cx-&St7sk'vXV/sJgi22TCh8.D6]`("
"9U7I)ZQsAFZ7#3'28hcH]KtT%gR?`%SP9U[vZ^T9aYR-)2kq`$bm,7&s+0-)J/vY><Gr$#_dWa=7Jd%bWj5'oZ4,871h]<-nPSJ&Id:D3ke.<-Q(gK%cM`3_$l0F_CpfP<,==:IMFjV7"
"Eu<#-QC=#-PG1j(4H0j(Ai<#-sYGW-kIL_&9`h_2q$<qi$OJt-e1nPJT].IGYR@k&9XDN9V9niL-arD3^ilB6E#[[-GqOJ(EZ[V@Ra<H*@2vW-jI7w7d&N7[RXR>#`4rC,3;jc3Qr058"
"8$;MkaXH##]p4Y#b#h%%aLco7oEJjru$n[&WYuu#bmpj_fj=WU_92,DfH;s%5jk;-pBcJ%MXQV-`C<d*5i?F%0#[w'T7Th(q0xr$Tj%B.ropl&%DPZ.,ecgLVas39xXTj(v3kf(UxUs-"
"&R-iLPikjLWtQlLh)9nLx4voL2@]qLBKCsLRV*uLcbgvLsmMxL-#5$M=.r%M]t'q@)X]<-*uhh&X'pVH>`Jwoud:e9U:EvQ4Fi[,YAl)4#mW)-iO#$%`.jt.$(L&82u?]k/g*u1CU$]O"
"[F:^HO*$)4f%H>#EUW-EXPL/>gq?##Up4Y#M63)#6Ja)#Qa'*8Kg<N1''d6#W4Kk$c]8^HWc:i:K:n^5njwu#pP.,)1)Et%SuD_&ksiU#`tt42MW`mLslGoL[sfqf<FpkL(-MhjhHFgL"
"0[c,M)dL-%4mC_&@Vh>e1iG<-nvR/M1^,G%$_P=7u4=1).#4Q'G#[)*D%NT/_JsF=W=DH*_55x65APh#:jE.3^q[s$K>L/)KG>#6u=rO'ExlY#IQ>N'MDeh(L`:Z#xWF9%b/M0(H/Pq&"
":CIw#g0`[,s78_:3T1T.0uaJ2<Eqs(Lx9]tP?YJ(S94]#nHMd*h:65/Y@MZ#^,%-)W_Ht-s1K#,(:nqUV/tr7JD%a+qXG.2=[ewuf/89$T(V$#'9q'#qk;/'*xb;?cj,g)#c8h:n;i/s"
"B;4/Mq;0s7?bNN1?4vu#tj>FF^5Qv$-b(($cce`*E4N>-R>`M'8^;[$F@r;$+AoV6L=nJ(F',N'Ul8M%%Vat(aZ6o=G`tt&?_vI:F]`U%g.iV$<u529InS2'oS#Nuh/w5.b:wBP%,o.M"
"iT<G<$s<#-K.=#-a/vB?jd?T-hBf>-6h?T-,_?T-,35h'L%ht6J]B.*%GO.)P[Lt.E9rwEF_Wp%x-h)N&89QcF[%w#BqNX$2]s6.P)?/N$oor6a6&3/66;S&m#^e$U]LZ#,G`?#91;?u"
"(c/7.lu$-M'aeVu>S*wptx9xtc`3s6C6w.:?_8PJRvF<-`?>O-3xtR-`1v<-gFv<-h1v<-HEuE<BS0j(-YQs->^jjLOJm6#'^>lLZ5Z6#wV)F&OiD_&NVoD_hhDU88#`UUx-L+*66g5/"
"CcK+*Tg+ZGm?Zp0:=)=-$/M0*SB+AuX&?n&;DcY#H=`?#l9rw,`wHX->4@W$iYV0(5L&m&4CC#$IsA=%jjpQ&7HG3'IaUK(#b4n62AHW-&JEX(`W6o=b&.0<l2c5&&'cx#KK0U%X64]#"
"_5@H)/:2=-F_IU.Sq7@uULZa$m<EW*AsujDb_$##IN'#vsxO:$sL7%#tVL:8F,P?ITaUx$cQ<r72LE=(3%*Q8,O5s.cB6s.9'Fs-b3A88->lD4LFObPw_>X&hw%=-^MVu(pDk`8OdDW%"
"StfM'0a%K1c5G;HO0@,2=CaP&Q^'=S2?sUQ?xHK<IF`2uj.'58;^v`*@_NP&m,u(3Ft158I)#+Nm%U9(=5re-cD^SU`Gm6#V4+gLQJm6#oGvf;o-Jp&#Feb$_DZ6#t7h._Ahx<(68q3C"
"PGH2_0Is'MFN+4%Dx-W-6^S3Oom$)*XR-5/T,YD4ehbP9uMk]>/4tiC?p&vHOU/,N`;88Spw@DX*^IP^:CR]cjlF_-Z9W$#f3M9.;=]6#_i=,M%*w##-r+@52,m[H2@ti_9%e'&=cs9)"
"]I27B1-3Y/HpX)<kdR;o_An90?dKR/9R_u9SpaYHMOB$-41P^$FFR8%pR`^6dKo+%0p,T.U.l($xZNR%+Nub%vYnP'Y^HD*_LI^$0%?=&o`RfLSe6##.p4Y#^$'2#Ex]E#+j2j&4>V,;"
"t[V8&J-`i%9H_=-QBf>-afXo$4rHE5%`81(Nnwfj1`78%%`AD*EOlS/U5u`4fq'm9vV0#?0=9/D@#B;IP_JGNaDSSSq*]`X+gel^;Lnxc+eR5'=#Hs-x'#)MeOoo@B*V$#3?8Q(N=sv'"
"Hpgcj8N4_Ah3QT#n-3)#ke%j0`._'#CS#<-+n3^F]'4m''FEs-J?G)M82oo@Z0@s%<5D)+GbL50WGUA5h-_M:xigY?2OpfDB5#sIRq+)OcV45Ts<=AY-#FM_=_NYd/'x5'?1xr$OpG)+"
"Z%b;.qxDm$EJD_&xB]&Mr@,gLZLihLDVOjLTb6lLemsmLuxYoL/.AqL?9(sLODetL`OKvLpZ2xL*go#M:rU%MRXbR-NU,<-^[lS.9ofh2>OeM&X6lj1XQo8%x,Tv-dEsv6;&hf1udn5/"
"sJ))3b6FA#`9OA#9)PZ6tFRv$l[K+*XF>^=F)TF4ap=p%NjLK(RH#n&]`je)+I)@,@-g@#8IKg2apA5/G%;5/.$),3c:M;$pF[c;k[dY#cJD0(J#eD*OAHp%D6Tq%OZ'u$c(XE*Spq,)"
"Z(as-UQRg)/D'#,dh5H3u[[P&c>;4'mmbA#xQt5&U(2Z#ohJfLNR-##?v=Y#:Z#3#t>FG'saa58RtZij]5Z6#,M)Y$W%1GD8@H[Rfta,<wQ>W.1)TS.avf;-+S,<-65T;-F5T;-V5T;-"
"g5T;-w5T;-16T;-A6T;-Q6T;-b6T;-r6T;-,7T;-KjA,2I*Jf_^>df(,7pK2vHuD#'9_s-Rq@.*@=@8%[L(*4vcMD3CcK+*x3vr-GF;8.jBFA#LuFI@^8Ba<MKXX$hCQ',9t]@#Uc1(,"
"S#6X$`,ID*m0)H*#dtv6]D?7&-?Kn/m6LG)xqr=QUT[g)U@$C+;mYo/g2ho/%/5##A2G>#3?K2#aG?C#pZg`&6sFp7RA=Q'''d6#tZ9`&WL+=(:Rgji77(p7V3$$$=.vu#%pk-$UJl-$"
"f%m-$vUm-$01n-$@bn-$P<o-$amo-$qGp-$+#q-$;Sq-$[;NaGMF?##ewlW-p1FAR>bhp7[g%9&m=;5&=<Xs-sK$iLOcbjLVnHlLg#0nLw.moL1:SqLAE:sLQPwtLb[^vLrgDxL,s+$M"
"<(i%MoT(q@P)+&#_NLK((Vh._;P`3_Yaj-$&aT49?#u&#'4'&+`x/c$>s,F%f12k)GVOjLWj0A=S#9K2a_1+*,ru4%b=@8%NNv)4)/lo;i9J(6E+]]4c9FA#bXm5/cP*5AU2QS/Sv5S/"
"h1S-)dn3t$:kfM'HcWf2t&ic)frv405t`BOlUH>#]9.L,r%qo.B.xE)M'YC-C7D?#/*gm&Nu.31h?rk+6kS@#I?BQ&)Laf1<XI%#HZ9^#lsC$#fEJs7+jNtA,n.S+<B1E<XeB#$@K'T."
"fKm;#wW2B%aZZ`*Ot1p/V>:&5g$C2:w`K>?1FTJDA,^VIQhfcNbMooSr3x%Y,p*2_<U3>d3?F6'MZIG)KT95&.Tk;-x$AF/Td0'#<]>lLfs&nLv(doL04JqL@?1sLPJntLaUTvLqa;xL"
"+mx#M;x_%MhQN7%6c@rm+(:^#>9WeF0)D-20T<h>?Fb&D:M)<%qr52'Nnq,MH>+jLX>+jLLWpvG-^L_&Xw5R*SWaJ21d'Q8=CMH*Y6xI*+5QT8I<oJ3sc``32Pr_,`(4I)35cP/SoVT/"
"O>hZ-SZlJ(C;t)>YB(K2(;fP'=EU,)Qw<t$F6TM'[==p^>2GB,v7KQ&$:6a$)mRf2sQ0q%,xrH(R6K6&7vIa'4f/k$lrpXu85>SRw>>G2x>ivB74G-&=5Dq%5BGJ1>1vu#c0xr$3;*^,"
"Eg^5/XXI%#@QWjLUh?lLfs&nLv(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M;x_%M#b2'%JIArm-4L^#J?MJ(Bc#9^E6Z6#kncZ%Y'Y6#<^Z6#L5T;-DuS:&m>+I-1WnD_s$5R*`a>ND"
":^.iL2SPqAvwsY-AP%a+BruJ2`4a/sVII`,eFXI)2>Ue$W`E+3O2'J3'F/[#l_N$6378C#MZpm&RV=k2Xq0T%bV((,bADm&aasl/+sHZ$G.ZV']RM?#VmxYG4=ow'F-lD#4aAb.-2it1"
"@OLP8h]im&E8QS/jCS1(-6*:.vR:%,I####,V1vuPVZ_#sE-(#o`xc&0bjs-lnCDFHSus0VK04&X3=x@`wk9@OW1a4pRbWq/D8d2GF;8.tPFNFfMMO'l0vN'*ml'&/thc)uW)A5SY1Z#"
"=Rap%U6tp%<^GS/OAQ6M:WaIhA_lA#;:fT%LTq.2^rG>#TfLv#Ydgm&JLi?#L/5##Nq*A#fha1#]5_g*<,N?>9v;@$v=o6#[EX&#b]m6#$UOJEE3>)4ix_5/pXI%#WQWjLUh?lLfs&nL"
"v(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M;x_%MF1i*%b9Brm0Fh^#MNif(Rs=VHj56?$rVCG2L*.m/b'Y6#N^Z6#15T;-u@^G&Fx#03RAh&=#ZK/)MhlS/Kw5F%6&P^2`MMT/R*kla"
"a/8u6/RYI)`V6C#GMrB#+%%],'OT&$Y.p6*G29f3G.,Q'WJ6C#wHeF4S6:V%iki?#K@6p.v7aX.t?aU.Owwt-7e-,*l83E*MKXX$%LVKMA(GA+oS;4'erRL(LsVT/.m_^#S(/8@R#_^#"
"51b/2pvab.+T1<HACp+MFfo1(/x;0*lWZfCnK4T%d^TU%o`t_j@5SiLv`A:/^HSu-$),##@p4Y#kFU/#[*i%%t0Da*Y?Tq.93pL_hAS?nq]G&#]e%j0(mQiL[icP9J]Ys.h9=8%vRkA#"
"`V=r$/#q2@5XI&,o7Cw-0.@G;tb5g)rKkY$EF;8.?`g58Y&#d3C;8d2stC.3iuTS.fMR-)&LCJ)-Nl'&Z>CKa:XEulfMMO'b)wg)`fnFM4AE>5`n?#msj&r.8l-q$b5=%tnVn]+I,w%+"
"<58$g/8?f_KWGJ(%bhA#3Uu1B[qb4'C`;]J-'&],Hb9kXC=5s.pBXs-BQRiLtY=D<>T+Q'GtK<q8+Dr7alQ_#6D-5&h>RA+h6el/?jh-debno%L?7j_jb5;-cjuA,&;Hb%7[R##8cVs?"
",@vi_X;S$91/Sh(KR*r$bgf_2ZQA.*ZqIw#OoXjLd=U%6M:xddK=tD#)$fF4<oF,kms&nLB`bU.xwxu.9Rp*#1'[,*ijER8t$33&]Ic;)f_L`+6kS@#+38x,xUbI)h@ml/1NTf'f4:4t"
"P,>>#Ex9xtp(mrHI/pO9FIH+<>bKS2fwFgjgVZ6#FTY6#hujh2%Y@C#oD4gLTZ61M'i+G4.aZp.axJ+*=$lf(ij1h(fl*.)DEo0:f%+.)_0=C&f*5$cLJ<h(4VG^?9$,on6dtM(ka,=-"
"ekg&#qp.m*@2R@-MBr@._wd1MoBgUdsRf+M:9xiL$;bW#@9E)#b?]Q8T1f,XDL8v$:vx]?lwv(#7Ku/:;K:&5UOIW-kIL_&3dc8.*qUpfPTa/26l&kLjh3PCx;BT9[Dm6#sHFjbQL-##"
"Xe%j0&mQiL0CspftqLP8*clS/_[E<-s[hQ8R;n#6,R(f)FXG6:-.r5/2Pr_,Xvdj85N7a+-9l'&wPKS8.bZp.WW,R&m742(1YdY#,MuY#x'Yf2eL%s$C(OI)^?OS/h^w&,MM'TpK@H>#"
"*NBW-1dLn3j<gm&Nv^^#k(l@$/WXU.T,>>#D%U=u@.3JUc*Oxbx-E<-v&Z[$Qi;H;4=GjD>7P2(1Zeg:[q@T&nFVP&E:[s-tK$iLOcbjLVnHlLg#0nLw.moL1:SqLAE:sLQPwtLb[^vL"
"rgDxL_R<^-v$3F%XLJj05r+@5XQ35/iBXS%tCF&#6Fa29Wa0C,V<SC#xkh8.s6,V/ATI901$W'%Ai3^,e>*H=Us0B#Fq[X-Y,vN'_muj'i[([,,/<J2pj1,)Mo*.)lpko/5UB1p(ikA#"
"c*%^$sg5g1E<bxuK5qB#'B%%#;8@x*wc^q7YV>1k]5Z6#ftRf$u/@D*_kHP/U5u`4fq'm9vV0#?0=9/D@#B;IP_JGNaDSSSq*]`X+gel^;Lnxc'x5*P`W/%#kWeQ/;=]6#OTY6#f?%X/"
"T-rr-xuED*3en;-L^KO&LjKcjckGQ'&P<j1aY8f3d/H`,-oOs6jS@l1ZKj?#lf/+*GTsf:0<3Z5YZs%,9qS@#An_^#.B/A,$r]j4P=WU.5Y5WMqAMG)7mSJjSH^4ThOH>#nP'N,J####"
"IT0#vUJ?C#lsC$#m&Gt7u2/k;%e`#&hVYb+Zl9'#,wRm%2M**<xf2j(pX`8.W@0:#+/k2;pEdv$<a_c2C1*9.3v+dj;0lof@sb;-2M#<-J3S>-@3S>-MuVu$N@t9)N@t9)SOt9)2MjSI"
"Kjk0j0PM=.2k]R/+'&J37ZRD*]q.[#+kRP/VaoA,bkh8.UD.&4ZK=lC.<&A,UE&&,5Z$V%/Z%x.fI0j(RfcY#V&Hn&I91,)<X1B#?/JiLW55',;9OI)ncFfMeUCs6=r_V$>WKq%;YcY#"
">l%%#E<bxuQ2qB#msC$#uV:u7H<x21''d6#AeoB&ahDp7ChZ3W8(5s67_xq$of*C-q$D);wnuu,)Oc[RaA.>%VVr/8/:*3N]AZ6#kha6#i4YG-7Y#<-c^./MGvRiLB4+/29AX9/W`Rh("
"&3=1):CJl)MK7u61k_b*GTHd2<MjD#NDn;%bim$$McS&$g0Ih,3(<',T,,',3%dV-h`.1(B(;Z#H5A>,2E:h$fGYb+*9vJ(sPPS/==:^4rTpi'H'.o[V<KQ&&Z4U$:oG>#TcCC,H<mD3"
"KN^m&$,>>#=M=%t0SKYP;jQ.qVQ'q.:3pL_(s(Z[@mR.MI2O+NS&An$/`<on`j)^#EDHs-iK$iLOcbjLVnHlLg#0nLw.moL1:SqLAE:sLQPwtLb[^vLrgDxL0>Jb-v$3F%oQV8_T=D0_"
"Wxcq;ufGq;d*Y?..ecgLM4<$#Wl:$#V5T;-g5T;-w5T;-16T;-A6T;-Q6T;-b6T;-)XjfLSRg_-57Vmj^Q&%#o?8b%V5;j0=Qto%5luu#fKto%7Y'Z$vhf20mujh2]v6)*;b,pM<a*P("
"QA2T.c,H`,a9&f$i`$w-G&Q^&3;G>#AqXs%/4p1(bU;v#K$9Q&IuL;$ib8E#c&hU%cID=QdDDO'?@]N'Z8[d)a$2H*Kba5&?/i^obkFJ(MF220<K>D353g2DI:x.2jO##,36ep@Ho#El"
"j%4a*]HBT.c@$(#O;`P-?5`P-t=F$%W+P?gHl[w'O+]w'D1PT#?^P12B:.[>u0O2(wE9mLr+HD-1mbK&N_w;-P<04009Yd+RR)20j;k-$Dmk-$TGl-$exl-$uRm-$/.n-$?_n-$O9o-$"
"`jo-$pDp-$*vp-$:Pq-$IsZ#8+l2'M_,q(MWCoo@ckGQ'w4XL<nuSF43+gm0J7e8/jBuD#&^&uIMMnm0SBo8%g,]LCx=Rs?bP2,)]91VIfMMO'U)&q/@e75/Fgwr6b]Gv&Uo[W?NFMv#"
"AOn8%l7aX.a]r.2ts:,)$wJR/dE)*4`Tj?6T9^m&[TgQ&SW$d)#i?>#:V9xt8dxxFFV`r?)Es+50o[9IIR0j_B=vu#5Fe;-a&?E&laN^2,b'u$#ZWI)c]LM:ri1T/pc)0;XUeA,N'`&2"
"a2D[,id#n&OC%<$Wbp+MviYM(TI79(0v^:.JK>R&S3B2'k8Bfh*XF[d9####=hwwu>6FA#(Sk&#t&1x(bNTg:79=2_9&I-FY-3$#Ef1$#E5T;-U5T;-f5T;-v5T;-06T;-@6T;-P6T;-"
"a6T;-q6T;-+7T;-;7T;-di]n$7f@rm)r'^#R(wJ:E_Ys.(V>>,ed@a*1f?W-CKO6q(5kmf@5v,*C%gb4G<C-%,r'tq7q<iMqb]L(jAqB#NGb@?e,D)+H..<-,wr#&>1Z$,k#x*NPtJQ/"
"H,R-)CL>uY#r/018IIwudkAE#pM7%#AP.R&xV19.+[it6UpK,*i?1T%[G.$$:jE.3c*&[uS,;0(Bjn`$/+;@,OttG)Sqew#P@f]&Zw`Zu%XK+*/-a5(U12Zu[%###.cCvurA3jLIUQsI"
":+K:Dm`mD<gksJ2YU+cI'LA8%Pq`,Me0=fM8oO.)eTZ3OqQbX$[g(]0q7)'6l@oh(/;wmM'Z(m;Hs+Q'`3X@Tb+T$j#m,N(3Mr9Vp8VS&4nX(0?&GY#5*02#lvGu;QM?v$Z#X^%/a1Q/"
"?]d8/[j:9/rtSd;@(s<%vQB99g%+.);63#&(<@V8Bd<I)Ch@D?STG/(Dgu<-vjLZ(>8hcHjf]8SCoF&#&+hB#M;3A+wjdxO?6Zj+5Eb3&wiec)/Yuu#xC^]+m)gi03.a5/fXI%#MQWjL"
"Uh?lLfs&nLv(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M;x_%MgG6.%WqArm-4L^#T8'&+LKEn0+fED#=Q>12wM$iL(fU`jB4e<C<I<b5-ecgL.ecgLR2oiLVcbjLtGNM%BY[w'=aEZ<"
"0_Ej1lH9H<=5*Z6R@3>f=jjb$gJ2;?)cLE4?w@@#QMcb+jZGN0JsGS/Z@=J2Ki0588dk,2>hH0)&dQO'Jh4J2]oG>#TL?_+iZ+A#+xm(FJ9LG)*`9oofMMO'RiCB#Qn6Fs$N-N01,>>#"
"UBT2#RCHtJx@eH+<Mi-2_AMJ(T5;E<19=2_.vE&%+XPgL-_YgL8^R`Ej?g;.C,#nEUakA#rKdY(dwiZu,D&[()3V<db;Tiutn7L(@,10*]?x[#*U&,2B$=xuK&h^#qQk&#BfAE#$H%q&"
"-BDWA_Am6#)r`8(]gCw.IBZ6#RTY6#t^(]$V$>onohuK3riu&#?lP<-F5T;-V5T;-g5T;-w5T;-16T;-A6T;-Q6T;-b6T;-)XjfLLCg:d0w[5'x#Ri$P`oD_mkD%eWT$12Hbww>DafS8"
"XG:<-NX`=-b,u?-XKx>--(xb%pL##,%%o$9_;>)4t.?)4']r]5bo$^53HW/)ES/&.Vu;9/Ywk8&@<Tv-e3^q%s(X7'R0o>#D6YQ#NQ'V.>J7[>jD?7&:BvG2Z7,3'M$@H)K?53'Bojp%"
"L+kW/@vs^+oi2g('ZKe$4Pwct0i7(##K6(#(;Z_jq1a3_pYU.(nQ#dt,65rfThUu7/3N;7f@N;7k:512P<Tv--E6C#KjE.3c9OA#^/Iq/&b#r%F+<q%M.Pp7@]o2Lo./=%)?^e$F####"
"e3n6%.aKs-hRe%@17<jr6.rx7P'l]>#'1U85v2a4)Y[T(`W]s$`h%s$CY@C#^*FY$q3mo7Kk7D,Ok39%J6tp%GUWp%-^MH)xlIO;Q^>7&5o2<@De<9%,,$68h5d5TXD`o&+cLP/rn1W-"
"2&VT/J]h8.wn'B+Yk8N0mQk&#<>N)#O7c'An;f#6[3wu#YO):[;/w%4-Fho7=hou5pK_m9Z5l6#`;Ycc`^8%#@Y8H9vC1k)-)mp%[D7g1Fr+@5Cvu12@oLejI3)mLdW@IMk<*mL[M%-F"
"8vOp8RMfl8S_i;-,S,<-75T;-G5T;-W5T;-h5T;-x5T;-26T;-B6T;-R6T;-c6T;-s6T;--7T;->@pV-M5t8KD^:u$KkRP/tkF+3*0<'mRUqm8U8jP0DoDp.ohbI)KHbe$/2NX(nB>9/"
"R.YA#,3VhL>)o.M:6KK1EJ4;-6]r=cL<02'gB5ki^Gm6#/1Rc;g^%T&?R#x8Knvi9Fx*#H(Sm;-)e#9%2e`s%^/MO'2x;<-(ZJK'7vxgk)<TJ1D'%29Y1m6/=,hO-).Dq',-VA-=6oV'"
"9HD]-G3,kD@5PD%UpYc2jRIY>T=UJ#lP^%'#;:8.Q<(F6pUPKahV-r%+:?Z$1_D78va@[H*<Gg1a@p^+]$nY#lL,m8f7V9/D'%29'b^o@r1N[f9wH+#Ef%j0wCw6itNZ6#5UY6#WDa6#"
"rM#<-@inub_Z=G;w%NY582e4_WOa3_TQ2F_<'&njR:WHm'_^w'+)U>?hngQ8L5c3)lDPA#Sr-T.na&],b.HO6FBBh*.B;$B7Q@&,0X5>.H1cA#xV@Y%@t?(#'5>>#bbEonYd02'bhho."
",2Lve8,6p%V;@a<aBtB,owED*e678.Z5l6#B0Fs-ogtgLd5WK('&[6#o]Z6#$us6#RF)=-JX_`$PtX4o14(^#(dc;-W(c(.D^jjLetQlLh)9nLx4voL2@]qLBKCsLRV*uLcbgvLsmMxL"
"-#5$M=.r%Ma*(q@(3sbjr<gj'O9FL:+Hd5LV^;E4Le)/:2BMd*44)%,ld<p7Dt9B#5:D@,KV0-'F`7B-nq?##+j+Y#u%w(#`Wt&#Nr$G;/;>2_/^oOV8W%iLD'T%#c_R%#U5T;-f5T;-"
"v5T;-06T;-@6T;-P6T;-a6T;-q6T;-+7T;-oYk]=Y2;d*CWg$'ub%<-P@0k$Ph=on*x)nq(L>gL;W%iLFcbjLVnHlLg#0nLw.moL1:SqLAE:sLQPwtLb[^vLrgDxL,s+$M<(i%M^w'q@"
"1?3b$oBL#]#3Hi;PYF&#@;m,*c`TP&IO#<.=P(,2UZE/2`uq;-bO[Q/=9eh2o'Jw#*XJ<-pf/n$r[NT/)[+T.,<Tv-':b$')*ic)WQj%,9qS@#QV((,;,Yd4A^'8//?s%,9bn_%I1@W$"
"Fl39%Q-49%O.i5K<r)=.CQE9A4XA8S#<Q1'Vq:w(]%sw$a-4=$s*f%#eSZI%@>AD*(sq%4D;&M.A^Z6#/]$<-XU`R+O]oD_1?sJWRC_kLlLvt7DC)kD&Ci8.*iU/WIRB=%Gc9^%;nMj9"
"XSgJ)Ka5?-=i:]$1O$(#o289vpM]t#brC$#40+f*2nZU/UBo8%`)YA#>Zk;-9h)b$^@i?#S+.uooSYA#T$3S;*N_V&GRIs$UsuN'D:7W$omLH)+_wc;+2@W$[JLweZFf8@(MKP8VDh^#"
"t6gm&F0'E<q.fpAa@ZP/L$^]=E9<TB=wVf:Nu$'5D'%29;%8/(3FF`a9bS(6;[tD#Q9pG;]b_W7Q9k5&(3$t$k*xP/CwAm&F+<q%(7PY.5;N20>>x^$7Dgl8^&J*+n1AD*/]/;6Ej1'Q"
"1^4s8w,nY68S)>-/8;Z[tX:+.1Mhp78sn;oov(F&BETa<Aq$H)I6,3'>V*t$GTc?-a5_](vLM'6.o.(#`Wt&#=GjD#CTVG/2T(,2iG'>Gr)vQ:FOZ6#kTY6#7Da6#W.r5/@1Nf_D&L]="
"*WX&#/+[)I6o^nf7r/kLGq1N('&[6#7^Z6#6us6#o?f>-.:)=-_4)=-.-am$kgY4o+Emgj)RGgL<^.iLGikjLWtQlLh)9nLx4voL2@]qLBKCsLRV*uLcbgvLsmMxL-#5$M=.r%M]t'q@"
"##W/;;jl)4`a&],6hU>,-s;bPY/pp.ST]$@lnZY#4D9xtQ#;,)&gU`351DTU&rZ12GG-W-s)sQNDoq-$^w=TUe5Z6#Bh,H&CDD_&#PVw0[:ZqApv,K)TOvqACjM(?)f=.)h)k*Ef*f&,"
"]Vt6fn$.nA5GlA#7f+S&0pao7fu5K)'pq%4[JgH-M+6&%hb_&?2$UZ$qU>>,=H2weL7wT%,$%A>/E;-*fgGj'p;Tv-K;q=-UH9s$2i=,M^Qxs$CSI,,&=`B+fm=p%Yvgu$XAYY#3A9xt"
"g0[P/,GNY5%ir>[=5&v71c6#6Gdoc*)A$<-oFIK;=#T&Zi5Ps'T7f40Hj5r%#%U@%FdGnCb):6&G+<q%dB?J2j13m/n^cn&xr2@$xwB^+qx8;-.Dvt$=(MS.:(0;6<<0QC+Qm6#$k9'#"
"lxXs:HY8:=*ejl/v%[`3HJMp.?1Nf_^xJ*[.^m6#qiL`Fl?=Q'/2gi0HsJX%82e4_6A`3_5Uh'`MGFe-DrNX(^b=X(Xuu8p.1o(#57n79rN1a4v@`$'Gvk-$WPl-$h+m-$x[m-$27n-$"
"Bhn-$RBo-$cso-$sMp-$-)q-$=Yq-$EsZ#8Hh6Qq^m_9&h;u=-a?Tc$N.RMLO[`p7[L0,N#BL;QV_?>#414`W58u4SsA/DESOqa*ko;30_rW^$k6c`*=er-$qp>4*8W%iLhpS%#HAg;-"
"V5T;-g5T;-w5T;-16T;-A6T;-Q6T;-b6T;-r6T;-,7T;-CwhP/I*Jf_EXrr$rtEs-QTWjLKWiw%[]d8/;R+Q/$`^F*%[*G4>Kh5/B.i?#*F,gLLCu`4QP,G4%_MS&a#ic)l:al0;kU,)"
"Sqew#-t`@,:k3',#`FT%LAZ@-8c)A-:$YJ(WWCg(X6uJ(5a=F+&E&u.PKV3(&G&3Kw)wC#i[0+*HiE/2/D[i1VoNM(=F7s$':*/14+rvuvm(c#:L6(#Cn:5'kfYb+n_BnBF%Q0+vf;x$"
"2I%7(KQs#&/<GJ1%S8>53$'T.FLm;#[h)b$+?Gb%6Bk-$Fsk-$VMl-$g(m-$wXm-$14n-$Aen-$Q?o-$bpo-$rJp-$[TQ&#Dt:T.HIo6#'$kF.dr+@5X&h;-*?/n$=5AD*'qIP/U5u`4"
"fq'm9vV0#?0=9/D@#B;IP_JGNaDSSSq*]`X+gel^;Lnxc+eR5'ufCs-j'#)McIoo@s<LS7@/w%4vRkA#Puna*HHBKCmU[A,lFnY6`HZs.?$PV6h5*e+rqgjBe[GL2#$`O'-r&02:GUv-"
".<Tv-/_2w$6Ou)4#IuD#(%,30Qu$x6ROr_,6K::q0rJd)H6=D<7='K2Jui^#,S(Z#D?#R&Pki?#]p'q%/b0Z$0842(UQRg)W^8#%w<]S%IuJf$(VQ>#OG*I)IY1Z#>oPZ,G_WJKpLdZ$"
"NDwd))C*PIoIit._&qo/$),##gJuY#PJ$h*r)SE-hC=2EY(qM)9'm_'XXMZuN%J@#eIsu$3K&n0h%;`aphpu,oW>W%$*#;H<&[IMfkG,*ZuMd%-w<$MJiBb*Zefs-u#kxF8m:T/5VTN("
"-bf;-+_FU%t@i?#wx(H*[224'@ZW_5?BM22J,I-)x&T7/&>7V?f's;$NtSM'(2=<6<v/W3)Clf(OZUK(OZX&#'1qB#@Pa(N*OWJ1]#'>G&K0j_AKCG)$s]G*c,mi'$p,N(D;7r@8lK/)"
"Z6@<%TekD#X^Y<UmGXd*4nGK1<?M22#ogF*K@u:Z'e6r@dWsv6[Z*&%waQgLiBi:%cdA%#0cCvuX-Qr#rrn%#dEX&#0P(e&Zl`T.$2+8_j1o?(p4<>-i:X3.9J>$M>9xiL/pdh2<x+gL"
"?IUv-E7ZV-f)>0NjTs?#cxnU/;)%x*_RKT%cO+onhag:%DknW$]>pQ&G$+=$_(uhdojhH0P@m>#6WBE<;G`V$>1mi9InS2'PW_KBD5YY#3MpXuY/w^f=Rel/2TEl37WA(%@t$iLYKE=("
"V+E<-JQ>,%7%E0_/K%iLHmxbeoA*lKxxfG3KTcd21HUD3.7D4%D4Vv#bijA+m=K[g*>sJ(f@+gLRS_Z1L4Tq%G$+x#Wop12UO2Z#08Oh<C7mTiMV@3'.=_^u77TU%GH:K(f+24#+LQ_#"
"2JpXub%T]ORe%/1QJHv[.lM%Glp[@>p2T%oS8^<Q^rV3;G$3j98V-C#fd`Y#X)Y6#8^Z6#LYlS.XCa6#qYIa*gM5-MOVjfLM<V3(2pxcjWtRP/4H)^4Y:U8i_Blj1&In=7A3YD#6N.)*"
"atr?#)qLT%2o.p?Njg`*a_#n=[/vN'k:dv#o)F8RwA+2&r*n;$<B?9McakA#g?2WTBDSN'xZ^K(P05/((]#r%,(OE&+tf5&)?khu7S@+3Tfuu#Du9xtt;`cMnBIP/n=]`*@c*q.23pL_"
"gvdsC`^8%#,X6T-1hSN-+R6T-wXkR-JfG<-RM#<-61[m%M^fG3r%X8/Qg]G3@gkM(H)lM(ed;U2P.]w'&9'd>_T]s$@Cr?#_2qB#eX-gL]6L+*+PTc-A%)s'l`)F3P_=V/+d0<7*[1N("
"Aabs$2l_v#nBf5&j4`p%?*^6&t$`;$_X%9%1fUv#DR>P.3sHD%Pp+$GA.;i%JIwS%`#TI$Qs:k'ugmS'Cu6s$UGLhLf#U#$MrC8%$PU;$ZPP@JV5Vk'anaXuNLDM0FjKSI20Y`3K9X@G"
"x%1j_PU$##4dlG*L@.3(g5l6#](LL&XH)M%YQ?+#]pdh2fBo8%exHs-j_lc*D(JQ/ZCcd3$F(E#>@Ha3qAGx6llE.3ig'u$Sq^+3oTeY#kkSa#u4/5'B_[h(tnx+3g+Q>#,`nj'K:;?#"
"c$W+5`.(V%Z.8B-Wd0p%>4iZ#ZbI1([/vN'hR_w,w_V;$Q$8U+pVGb+pirY5OoXn*O5YY#F%:xtso)/LRSBj1d;?kDG15s6Ljec)d+m;-Jca+0e)Y6#:^Z6#B>pV-`Dm0sGD4jLbA=O%"
"@;D_&,HnD_%3/F.u%@<-dGd'ZINx>-ZKx>-ZKx>-=^poLo22TLg8eh2L^0#$an@8%_2qB#cAur1oZFU%k?.H)eV8f3?Wm$$>Z)F3oq4D#HWej'L@D?#,E>^=kGW-)TYg3,B<5]#[1qY#"
"?jJb*9>6)IwB*?#$C]l0?g(>$QGp%64cTP8X^cJ(<,;#$P(T2%HCSYcas^q%Vb(d*&5>##?p4Y#)B9sLl2p%#s@>gFgJhj_7lYY#`Z93_i'R<_2)0F_Y#,F%eeL<hxs+F%C,-F%<%PT#"
"7(BkLRtJfLL1CkL=qugL=qugLjp*&FLWpZ-C%gb48BXq.kWHp-kRj'on?XA#jpB:%Uql(N#3uD#&8o]4v+xo%[][-)@v2bnwp,.<&nnY#SUbF+>/V11?LWp%ZZg6&[aftu@SvK1Y-A@#"
"`wX&6xsF,210g;$f2Pv*d58^+I4r?#o?lN1>####r&niL'Q6b#Vxp(GV8+v0DL5s64;SC#OCr?#an@8%9cYs-U>2Z>gncG*^paj1EHuD#FVYmU],E.3)6]_5KjE.3mvE?#48I@$8oUZ#"
":%OQ&RTpQ&SuToMA([F%5u?8%p*ID%77*p%/r:k'SD_w-^=_sLH+F>5t(DWHuH>n&q;-&4,I28&%ro6*[:,H21]#r%Z`ZPJmX#r%oi:(&R;_<.+%B&&E)]#$^<q^f6it^fqwP?g;&k7["
"@aFhL3cm6#r>$(#b]m6#NtRNM,u[(#h,=(Rw1n6#2GY##*r+@5T(.1(Ot*gj$'ai9A8<A4EOlS/l@@<-AF_G&od6R*/QnD_5Ut9)`(P,M9E@*N8XHA%wVf--Ybt9)<]h9;,R&F%e-u9)"
"[lgLCITgv-b[p,3W[JZ$Din$$XPd_&0RUF*,39Z-v@#c4s`J>,p49f3%@S8%=J&v5KbTV6_M_$'O+?^t4%@8%27if:'O+A@GTeb4r<]@#=72Zu0uUv#B5X.)R_;',/c_g(o,MD-pNKR)"
"T'][#'iWt$L8Q89gv=o/_'x[^%$fE+?WR-)GHT+(+fgB+9MHS/=Ow5)w@Al'4sku-;K?D*Es,5J#MPS._OX&HP(V&&`U)##,Puu#iZ95&hM,F%Y3l6#%L$iLlAGOM#ArB#Y+8pCQM=O%"
"wpVa4H6J9iE2'J3I0U-)2>_,H::9F%@cOhL76S<(qjRvP1(eS%q=jt(CBvcERf7u-^SRFMAYf`*0l^U%b(hY#-V:;$[R>%tMt^cMK>W]+tJbA>aNc3XPmt(<`9<bRbUco7?>V)+k]>F%"
"R:&Q/3YMY>HxOV6T,YD4+QB<-tmwJ&G%F#-v1]jLJHwA%9=Ww0ht]w'>B^&MU20+%59c$'a[A@'OD^MFGOo%[n>GA#7R+Z6TUm[P_2fJjeffF4bKIX)YflNk0+uH)78_@M*M>s-ii&b*"
"+cc?5F8N4'Wd53'xOH;'dH0q%a1/[>*9U6&a<t63'&OI)Cvu8.X6Bm]?klE32r3j1No73'IFW*,sY8b*>pP.U1x])3B?M$#FBkxuAY<p#6_Q(#'O7(8f><+Nm5+=&DTKqBHh$29Jm*)*"
"''d6#(Jb_$=Nhi'bmpj_1Jnx4W+Ds-KedIM[XZ##1C])<N?6$$q3'&+^L##,X8YD4ZJ:&5s0k(5`:SC#iiWI)I+7s$ikkj12$<8.^tn8%4j?T.cf]+4s8t1)%hs_5Z&PA#8fnU/BP,G4"
"ae_F*N3>V/mbOrd.qx(36IgU%HhIw#,T2%$Me3T%^3fNMnp^M'dC4gL4H>,2)Ph*%mhsp%oOeRnERV;$fYk>#0gLv#+@$_u^ErV$MGNBiPRMv#Gu1iM9@?;-=+.s$p#n<aOL;Z#RLoUd"
"Rhi$#$uTB#POqXu*j%)Nd.A/D&MNWHT=8C6Fw1e&t?3-vEP0F_X=OT#%ld.2G:e;-7V+5XPHMiLPEObjj)Y:.s^v)4:E*nsIA*F3GFTj03Mfl>^f3.)s67=-CVDL)==*T%8<WT'WO2?u"
"UsmIMx&OX*mqHw^^5X@t'b:DNZTI`E<sxS&TcCW->S0i:8##l_EwNP&e1Sh(j:SC#jpB:%D0pi'S5<m$ep:(G2B(a47-2m$/V:8.jnJE#$(6?$Ut3=$Wova$<QFj31Rc3'J0bX$qApS%"
"sH>.q2iU;$+9O8R^Ztp%iVnT#r^9Y$:lYY#<_H9igP&)NgjXc2:+)ri&7op%g4M='oEWa*;t,X.a9W[,=D72.RNjd*%jq8.awr?#s3f'&]@i?#@9:V%3L(E#'jE.3Mg-Un?FKAXJweW$"
"DeR<$AYPR&C44ReV=MW$Fh8Q&w$3/4^M;k';TrG#b02O%O-tp%tceA+NV++'hv+F%bUVmLI8P98.SG:vbwVv#ga$[`UZ1,)ww+B$?I<5&eO(w,$,>>#9S9xt`S[cMNV1i:N%QbYG[1F_"
"munD_aUOT#*4</23ij-$A2(9p/XPgLAXPgL;klgLCE`hL^KihLFWBcjjeJ*&pNv)4Q94QLKULP8Ym5R&N:'-MYHS-)v/vN'aDWhL']NO'.PlY#B&OI)c[,r#%^[h(9=&(M8rIiL(+(i#"
"#f0'#GlE01>3pL_>7;;$t1]6#s]Vd,_H]s$axJ+*Tm]G3FduM0_x;9/lHSP/(Jl/MDQ7f3/_Y3'wW+U%no9gGD4I8%pCpK(kp`IhH%i;$w9e`%v4LgG]Zo&&<M/I)#A)&JW*+t$u-6x1"
"_QK:%%)=6&6%J>KxeJfLQ_6##L#Pu#rdW1#@*xe*9p+b*#hNq7G6c59.q9D<S-PjD36V-Mk]j39;/@m0Js;q0Ljsn8N)tY-xS(6/g<7f3V0`q@ap;Q/[`JD*6t'Y$$]E<%#^c8/WEO1:"
"Jw3T%'v.v5cW>&$L_w8%/=pE#/8o/16[l3'+G)&JF$t5&aj[Fi+mXo1O('q%)Pgl8,m(D)P5Mo&)#A'+?R@@#ap@3%,@5q&-V)aI1GG>#SAc_dZ:GZ#.A>>#C`=%tXqfiK5CCP8<<RoJ"
"GV)aei(Hf_xWN`<-TbA#eKvZ%w?S8%ocf;6SFu)4s>17/KjE.3=DEb3Ofs]'e&Z3'<tvE1V)V66`>TP8Fnn8%r<*A)rDZO'VktH)sA=ci$i^j&?(JW-b;mv$3`u>#Xk%w#5k(G#Bs.%#"
"EJ4;-@Lt7e#/tu,ri+iC].P@P?r.NO@M;Q&txC0_.41F_pf,F%D/-F%B)-F%q,O^2u9OA#v.2a/a-ji0>x;9/''Lv6sRrdm.<MG)v=j%$bke@#q8n0#ifE,)mxLT%&=#F#)D7j'&C5h("
"LoC>7.o3?7,a+QL^Jq^uq02Z#Wx=;-94.<$i@o-)Er1v#O.96v$-;+r&Auu#b'V=uR?Z`NY7PV-[#*4`#u5x%o2mi'<+vu#&]>>,%2-F%Vk%v#K#xP0[$'0%bF4X]3N9F%3BJ1C0Q5s."
"l/Ha+0Hl+MP>+jL560?1DNVN($F.J3+b]R/s`J>,l(9f3d@t'4BOZ6ULh+DW#sa)-1O8@#^>B$&XB7b*KTwI0o7h1&a7Jw>Uh5n*1g>W6a5td*Zwmt-ad3p'K2,wPZnLB#>4<IRxI]1("
"b+[EnvLU=uMBTEnS>R]4BID+EJY0j(E;ZT%n47U.[l9'#VjA7.t>,LM(BVhL?esejV4GI).U7x,H,wD*@/_R'ih^F*kHL,3q_P12dp5V/O=[20<E6C#:5Rv$qK5x[ZH]s$xCT/)d5b/2"
"]BvJC5%f;Q.FeY#$$#$'TVvPJ_M<5&Ik<9%jECE*wD+JG<xH8%p>Ms$Q-8[uAbK+aTLuJCskt$%>J7w>5LH>&6uhv#uSGF>3Y:v#<*bCa;)5v#b:j2Kq-)4'xNNA&S1D?#SfsE*2'K#'"
"of?pIpaNI)`GE]+fk,NBF6nd)YF2>GUTT6&1R1`$RKTU%+?uu#xbLP/g]OVQtgEM0BIODOUXrm8L(CW/a[sC=L86?$v=o6#xU+41b]m6#]l:$#`PZ6#Rsi2%<&Qg)/>GJ1QtlS/LwhP0"
"Va*n0X)Y6#;^Z6#3>pV-F0K_&Fi?<-ORG-M,fow>BwnP'4r$L2s?T:%B4r?#bMm?$(1Im%9`]('Ni^F*]9k=.+3xb4Onc$(5u$<$nsh;$$7D/:`pmT0*5V6&v3@s$Eew;.PTx4E;j2I)"
"nQD>PxDN@thVVO'cDGF%g)gU'B%Is$'@`7m_B^Y#b%uL^qkAJL64GS7S]n4'6V=nJ;vk&#$/]6#SD(='%(wu#gbPhLmqtjL)EspfAeGlLVGGrfsPP^,T-6l.]AqB#UGT[S>;Cv-?D,c4"
"8ijj$d&B.*[k[s$K&*E*m$6R*YhZV-$DXI)8fnU/:>/T%e*4A#9*uf(kgT)3DL<5&RKBq%pXFf(ntWW8KKT6&%xS>(_DvP0@qQH7aInY#>Ln>Mj629NYo[U/=bsP&@=WP&_jB6&5xNO'"
"J>]Y,Z(0'+N&VO'6rU;$t8TP6-(%W$i5b6%-)BAt]F=0205YY#J+u[t(BHJLw&'/1ZS-q7D6.<&p2^%#$f%j0vP.e*T<^s-`%pb*f:@T.(vjh2nM9(%;#r;7ZcKs7W#=#-rliI4xrPA#"
"N2xb4w7TQ$kYub%xwXgLehw]GZQRO'fMMO'=MfvP3c:v#pu>`IwuL+V`q^4a1mb8Rv?N9`B.v/G@&wH)5cTP8+[$EN;OEp%.2$HHVD?PJ.MA.#<TZ`*(d?8@Df+58_[o_oY.dE(6MMT."
"_(U'#,AX3.[-<JM6.W0($qMbjvGqhL,V.)*s#*^4w#9Z-Fj:9/;lm=7*h;W-;iZx60hZV-Mmg5/h]DD3`OvqA+1T;.`'(Z-$@wq$4<AjK.AM2'uD[5&Uuj.)`mf8%G*'6&X[]]A5g((?"
">r$s$xX;]=au;6JLWcY#v^-PfK?Bq%O'n)E2LY>>;*/s'5(.<$m(Fe%38cn()L[H$[g:0(:hAQ&s$U7DWUkX$5sj=&o.79In[u/(Sh/6&`4-_%<8LT(HCI<$(su##*C6C#>P=%tn/%)N"
"do@D*PeaWq#=78.*(L#$hI=j$vl2'oF*rhL*>?aM3?WB%4;h;-^1Q;D+.YI)Rr7S@D9Yj'SlC.MEqbU&UjFZG[puY#r4e)@hVVO'/_W/1o$$&%vX<qIi-RH):C51%5*D-)#v9gG7^0W-"
"gQXwTe]T=urA@DNA$@M91PB=?sRF2_<PotF6EwM1(+s[GDMQEP]<]s$p$WHDwFv;%hK^L;e.m,*SgqR#)Wv5/o>P>#'Qfp$&%)/:p?>b#KOwS%>1Mv#07*.7'rf2'P&7-)_N,q9HE_s-"
"mk1Z#AL<T%O0]8%]wIJ1=o1v#L[;ZuY(PY#kZ0Y$i$:dMf`.iL7cMn#wFX&#@u:#,?9Xb+)$if:%dk&#Q52/(p8-*+r<BA+*`)JOP7LkL9BW>&TK*I-D`[w'.mg_2`A)T/S57&4'jE.3"
"2%p+MbX@/(r+)<$Bx+3:Ik[s$A1L<h;6QO'<d*/MX0a;$Rf=;-'@mk(H(2U&W&bu#(m*.);U+@NhhpvLmT-##8p4Y#3=C/#a8'q'aK,=(W]hs-`h5GM%8it6J]B.*f9Dv$x9j;-ZoMY$"
"Kr.a+wJUV%m']='+4#v(AP>R&;eJ2'RP<P$AxUv#_h8>cGw<t$G$+=$T;l0,uBSYGNhMZuKP7]k=YG>#0T_s-Yr:X$>'gQ&)@1b*B0pQ&&EJJ:;J###57.wuCe?g#-0UrAnqgj_bdSS%"
"bmpj_dA,F%M%Th(sBXS%.Un;-_/%QJv=ti_<deL,VhBZ-,mjl/bH.m0+nM,Mf+:kLYHh02Ae(.N9gh&T17^T&7@[s$#=TE&9:)`+dJZe%8pl'&Pt-c*?7Xs-wrQmMtju,M/p3oXp0(-M"
"o_Xl$lj3hu16IE*>Xbv.qOOa*4`Cv#rqZ+83aZ_#jH.P'Z(i9%@@M?#P;<jL;EqQ&)FL'+Hcq8.Yv2a*,8b>5Er6s$RN'B#rH`?#$),##3^oX#RZE1#J,@UL2W>H(r6am8PB^;.=Kxq$"
"b:AC#?n:$#de%j0515##,`Ys-at[fL4?@lfwAhhL(ZZJ(%Weh2%6MG)?]d8/K@id*.)fj0[j:9/Kf@C#?;Kf3C.U/&nQXA#oh#1%7VZA#a&ID*EL4+)uIw9Is0vN'Gv3gP;JlY#=0n)*"
"'TrH)UrK^%0(BW8En3t$;*/K:S9&7LMmAT.dDDO'<Q?T).Q$9.3S8NKtXRw#?vPQ&*-XI)V74=?Ta8lbPOMv#&ijN'Rt5$#&+hB#^qU=u@L38I)60G5270DTN&oQimW3:Kg7Hf_GkRS%"
"bmpj_97vW%c+Sh()Nif()uls-:8Ve;5>]32j[D:8]i:4)WOAc9Y#;T%2H.'$L[es$sHK99Jtw8%1u#m81Rc3'J3bX$2kfRnU#3a*B@.@#_^9Y$u*fRnRfh8g9#Fsu7JMBoi&L-m(a:-m"
"d,./Lj1]`*tw&q7ufX;f]pdh2&r/l'O_i;-=Uob%k@i?#Po&02_S&J36*YA#@mu%=;N(KC,>o:d'-cG2D<53'M4r4)V:FX?Fwj5&ioD@$gA%h(YXuE$8Y1v#iPIE-gC&/Lwq(,';/MZ>"
"/4'h(Wc$Da3g(?#[a6peh,-R&p0Gv#>=cjBuJV$#A&sx+o.RcVpVwx+MEQR_fR%h*x.N<-wM8b%^6io_g35A>pMF&#PKKp.wCa6#tYWR_d@%N:gu*<olWav#:mUD3q(k'+uj)igN$][#"
"R[Q_&.D9N(`8?<E_YGW7s#PK)Q[*T%T(epgeZ8s$aA=##x%###ih8*#&)2G#lZJ7J#NfA#(^wh%`IR8%<cJ7JF^ld3twC.3C$^]=nW_G&p=7`,dA'e*sPA#gAvGG2hAQR&hl%P'Qt:j("
"XhHX-^eK7JR/vL26Oet-V/5##@pO:$Y:C/#<dkI#LWOk)1wJp.m3pL_Q6/:pJ_gw8,S]H<dKwu#%S##,a?AF%5`5;-1qR5'acWLD82e4_KxC0_s2618EMc##CFqhLE]XjLUh?lLfs&nL"
"v(doL04JqL@?1sLPJntLaUTvLqa;xL+mx#M;x_%MI2O'%^-Brm-4L^#LR7GD,AWNCh8;JCd3l6#U>qb>1#-v?E)Cv?G0+IHqfwKjFL(H-IjO?-45T;-D5T;-T5T;-e5T;-u5T;-/6T;-"
"?6T;-O6T;-`6T;-p6T;-*7T;-IkJGMjx3'M_,q(MXFoo@?Eqi'B+TMB-VeLC^^kc4S,m]#^gZ)*qSID*wK(E#[q[P/_GUv-a7ffL;ZGg1od5E=bwlG*J,_s.%(%]6AdIh,eF/73Qc[F3"
"wHuD#%]'p7SY*I)#Fd,*[oen&v_'N(]#OH*_?aU.jTQF#3L%K#dr-D,:qS@#SIZ5/9x&X%wCWP&'X`:QuPL11pcAv,Io3f2Dba5&kNx/3vI8JLrXM?#`pJ)<(^ic)Ota9%>[a5&P3]@#"
"SM^M%+j6;%cXH5/n-kT%>CBg28X9e?o-A8/hfCB#YrC(,qt(S&:F2s-EgJw,'W(sH`dA[uZJcU&^H4F+4g>A#AR6^$GlvK)U-w^+$CRP/&p;s%,;G>#G0x%#$,Guu;c53#:1;,#IS%K8"
"^jrS&4Srp.$2+8_#[CEQcPm6#2Mc##<bt9)eTY6#`dL]$t,@D*^hHP/U5u`4fq'm9vV0#?0=9/D@#B;IP_JGNaDSSSq*]`X+gel^;Lnxc+eR5'LJHs-J'#)M[4oo@=F1wgb(=-?+Fde#"
"):_/)q`K<7PZUg(*Pt**9X$c*Y^?n/FHkA#I,(n#UA#R*%amERpMmm#)=_/)rfgW7lO=,M?B5i(fw[O0#k^m&;JV^#%Sp'+oK<1#6_(-#2;$C#2j,r#Bl71$x[A&$gOoT$6H>f$od8e$"
"4W8E%_2,F%``pU%VZ-4&G-EH&`teh&/j]*'ouxE'px-['rmOu'M)$G(w84M(hgg')kZ3A)jKpU)NDjh)R0St).l***4ijX*qOlf*RhP;+00x5+e9ZC+*g*e+bf2-,qsNE,IFQ0-Me9R-"
"SZ$[-n3u'.5R;d./ee5/j'Hg/@_P?0<f>c0QJw-1XK+F1CcB71SEXu1mYM-2pO/u2kqn=3T.X63wskF3IX.&47PIx4)ol(5iX<m52J-D6:Y507gE3m7*I;58$o[e83a$59C'a]9WIH):"
"(__s9^1v4:pHxQ:`<om:A&T/;t%2L;_92p;&IO/<?WAF<Yx6x<P%B7=6bPS=oSFr=c/o6>NBX/>4b^C>:`fb>W1Rx>5*Dp?<0%.@'####O#.8##M#<-v@g;-745dMTF6##aR9)NbL?##"
"=($*NVRH##JU@/N+:#gLvYc0N,@,gL-S.2N[qv##v@N3Ne?<$#k@U/#+MC;$7F`t-P7C9NTF6##_tH:NUL?##nts;NVRH##2t;+N+:#gL]n).N,@,gLw`l0N[qv##3JW3Nj=xp&N2N`N"
"Wu5H=.wZ6D.gcdGpkn+H5-xF-8-IL2iapv7fa4,>H=EMMoOQ-N6)9/3Muh%6UOLS:<EIL2WUX>-aXQo3NK+9B;OkGHZ*L:B=(x+Hx]BSDE7]+H3Ww<IMn4RBl5KKF3N%FF:tXnBr_l'G"
"p)AqChpHY>YT)58bgdo78h^NCU'AVHF,w1B/]8R*0=Ne$.5PcDVU-AFu>Yc2,a>G2[`H#6l?8X1IaHk4I%jx=[MNk+N1d@9F1ix9^Dv(34rqg2A7ku5aP9G;vYxM1'xCX:i%]>-_@v]>"
"m`*3OpoJF-rT_H/NH4.#=#krLMKG&#%)m<-J22X-51hKc),WJDUVX>H%d_w'Y&$,2?#K;Iu=#s-6aqpL^:SqL6xc.#2Z5<-9qdT-TqX?-X%vW-`;I4=2^>R<gDSfCSvo,3%5YY#t1TV-"
"$W^KlChGJ$)faw'<4io.bo[`*VxJT.'3h'#peh).]8^kL`(J(#7dZ(#;pm(#=h4)&8U<;$Ls_c)N5[`*:%QT%7ssx+_4TY,cL5;-gelr-2uqr$'+p=YLwGS7u.ef(D<,/(4=35&:[jl&"
">tJM'uO'58&?8W%wx7'fEEm:dRdv.CPTU.q]>s+DGN1Vd*TRrZd5*qrBk@M9Zv[i9`;=J:dSt+;hlTc;l.6D<pFm%=t_M]=PFi3Z8l?1G]*EYGbKAVHLoOcD]Hx7Im/YoIZO1DEqMUlJ"
"wl6MK%53JL+(X1p,VC;$0o$s$41[S%8I<5&<bsl&@$TM'D<5/(HTlf(LmLG)P/.)*TGe`*X`EA+]x&#,a:^Y,eR>;-ikur-m-VS.qE75/u^nl/q028.J@Hi^ThmfG(4SMF&h<M2]lu8."
"P`Y(5`SUA5&RY59%&RF%[+RF%^1RF%,CK88s$,p8mtbP9iUS88iIP)NK$g/NK$g/NK$g/NK$g/NK$g/NK$g/NM,gj2KWhS8LWhS8LWhS8LWhS8LWhS8LWhS8LWhS8Ng6p89SwA-MVwA-"
"MVwA-MVwA-MVwA-MVwA-MVwA-N`<^-_j<F7NVFF7NVFF7LJ4F71Eeh2>4n0#S/>>#2[oi'$,>>######&9R`$[@$##";




void DirectScript::onTick()
{
    ImFont* font = nullptr;
    if (!isInitialized()) {
        m_StateSaver = std::make_unique<DXTKStateSaver>();
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGui_ImplWin32_Init(ClassPointers::g_DirectThread->window());
        ImGui_ImplDX11_Init(ClassPointers::g_DirectThread->getDevice(), ClassPointers::g_DirectThread->getDeviceContext());
        ImGui_ImplDX11_CreateDeviceObjects();
        auto& imio = ImGui::GetIO();
        font = imio.Fonts->AddFontFromMemoryCompressedBase85TTF(vibrab85, 18.f);


        ClassPointers::g_WindowManager->pushWindow(Window("BigLogV5", ":eyes:", ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize, ImVec2(10, 10), ImVec2(600, 500), [&]
		{


            ImGui::TextColored(ImColor(255, 255, 0), "BigHookV5");
            ImGui::BeginChild("ChildLog", ImVec2(0,0), false, ImGuiWindowFlags_NoScrollbar);

            for (auto const& i : ClassPointers::g_Logger->getLog())
			{
                ImGui::TextColored(ImColor(231, 76, 60), "%s", i.get());
            }

            ImGui::SetScrollHere(1.f);
            ImGui::EndChild();
        }, true));
        setInitState(true);
    }
    m_Save = false;
    if (SUCCEEDED(m_StateSaver->saveCurrentState(ClassPointers::g_DirectThread->getDeviceContext())))
        this->m_Save = true;
    ImGuiStyle& style = ImGui::GetStyle();

    style.Alpha = 0.98f;                                // Global alpha applies to everything in ImGui
    style.WindowPadding = ImVec2(10, 10);              // Padding within a window
    style.WindowMinSize = ImVec2(100, 100);            // Minimum window size
    style.WindowRounding = 0.0f;                       // Radius of window corners rounding. Set to 0.0f to have rectangular windows
    style.WindowTitleAlign = ImVec2(0.0f, 0.5f);       // Alignment for title bar text
    style.FramePadding = ImVec2(5, 5);                 // Padding within a framed rectangle (used by most widgets)
    style.FrameRounding = 0.0f;                        // Radius of frame corners rounding. Set to 0.0f to have rectangular frames (used by most widgets).
    style.ItemSpacing = ImVec2(5, 5);                  // Horizontal and vertical spacing between widgets/lines
    style.ItemInnerSpacing = ImVec2(4, 4);             // Horizontal and vertical spacing between within elements of a composed widget (e.g. a slider and its label)
    style.TouchExtraPadding = ImVec2(0, 0);            // Expand reactive bounding box for touch-based system where touch position is not accurate enough. Unfortunately we don't sort widgets so priority on overlap will always be given to the first widget. So don't grow this too much!
    style.IndentSpacing = 21.0f;                       // Horizontal spacing when e.g. entering a tree node. Generally == (FontSize + FramePadding.x*2).
    style.ColumnsMinSpacing = 6.0f;                    // Minimum horizontal spacing between two columns
    style.ScrollbarSize = 16.0f;                       // Width of the vertical scrollbar, Height of the horizontal scrollbar
    style.ScrollbarRounding = 9.0f;                    // Radius of grab corners rounding for scrollbar
    style.GrabMinSize = 10.0f;                         // Minimum width/height of a grab box for slider/scrollbar
    style.GrabRounding = 0.0f;                         // Radius of grabs corners rounding. Set to 0.0f to have rectangular slider grabs.
    style.ButtonTextAlign = ImVec2(0.5f, 0.5f);        // Alignment of button text when button is larger than text.
    style.DisplayWindowPadding = ImVec2(22, 22);       // Window positions are clamped to be is_visible within the display area by at least this amount. Only covers regular windows.
    style.DisplaySafeAreaPadding = ImVec2(4, 4);       // If you cannot see the edge of your screen (e.g. on a TV) increase the safe area padding. Covers popups/tooltips as well regular windows.
    style.AntiAliasedLines = true;                     // Enable anti-aliasing on lines/borders. Disable if you are really short on CPU/GPU.
    style.CurveTessellationTol = 1.25f;                // Tessellation tolerance. Decrease for highly tessellated curves (higher quality, more polygons), increase to reduce qual
    style.Colors[ImGuiCol_Text] = ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
    style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
    style.Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.00f, 0.00f, 1.00f, 0.35f);
    style.Colors[ImGuiCol_WindowBg] = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
    style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.10f, 0.10f, 0.10f, 0.00f);
    style.Colors[ImGuiCol_PopupBg] = ImVec4(0.05f, 0.05f, 0.10f, 0.90f);
    style.Colors[ImGuiCol_Border] = ImVec4(0.70f, 0.70f, 0.70f, 0.65f);
    style.Colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    style.Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
    style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.90f, 0.80f, 0.80f, 0.40f);
    style.Colors[ImGuiCol_FrameBgActive] = ImVec4(0.90f, 0.65f, 0.65f, 0.45f);
    style.Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
    style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
    style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
    style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.40f, 0.40f, 0.55f, 0.80f);
    style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.20f, 0.25f, 0.30f, 0.60f);
    style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.40f, 0.40f, 0.80f, 0.30f);
    style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.40f, 0.40f, 0.80f, 0.40f);
    style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.80f, 0.50f, 0.50f, 0.40f);
    style.Colors[ImGuiCol_CheckMark] = ImVec4(0.00f, 0.60f, 0.90f, 0.50f);
    style.Colors[ImGuiCol_SliderGrab] = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
    style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.80f, 0.50f, 0.50f, 1.00f);
    style.Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
    style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.40f, 0.00f, 0.00f, 1.00f);
    style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.70f, 0.20f, 0.00f, 0.83f);
    style.Colors[ImGuiCol_Header] = ImVec4(0.40f, 0.40f, 0.90f, 0.45f);
    style.Colors[ImGuiCol_HeaderHovered] = ImVec4(0.45f, 0.45f, 0.90f, 0.80f);
    style.Colors[ImGuiCol_HeaderActive] = ImVec4(0.53f, 0.53f, 0.87f, 0.80f);
    style.Colors[ImGuiCol_Column] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
    style.Colors[ImGuiCol_ColumnHovered] = ImVec4(0.70f, 0.60f, 0.60f, 1.00f);
    style.Colors[ImGuiCol_ColumnActive] = ImVec4(0.90f, 0.70f, 0.70f, 1.00f);
    style.Colors[ImGuiCol_ResizeGrip] = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
    style.Colors[ImGuiCol_ResizeGripHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.60f);
    style.Colors[ImGuiCol_ResizeGripActive] = ImVec4(1.00f, 1.00f, 1.00f, 0.90f);
    style.Colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    style.Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
    style.Colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
    style.Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
    style.Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);
    ImGui_ImplWin32_NewFrame();
    ImGui_ImplDX11_NewFrame();
    
    ImGui::NewFrame();

    ClassPointers::g_WindowManager->callWindows();
    ImGui::EndFrame();

    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
    for (auto& func : ClassPointers::g_DirectThread->functionVector())
    {
       func();
    }
    ClassPointers::g_DirectThread->clearFunctions();
    if (m_Save)
        m_StateSaver->restoreSavedState();
}
