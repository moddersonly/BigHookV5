
#ifndef DIRECTSCRIPT_HPP
#define DIRECTSCRIPT_HPP
#include "BaseScript.hpp"
#include "DirectX/StateSaver.hpp"

class DirectScript : public BaseScript
{
    bool m_Save = false;
    std::unique_ptr<DXTKStateSaver> m_StateSaver;
public:

    DirectScript();
	DirectScript(const DirectScript &obj) = default;
	DirectScript &operator=(const DirectScript &obj) = default;
	DirectScript(DirectScript &&obj) = default;
	DirectScript &operator=(DirectScript &&obj) = default;
    ~DirectScript();
	void initialize() override;
	void onTick() override;
};
#endif // DIRECTSCRIPT_HPP
