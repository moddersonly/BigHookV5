#pragma once
#include "BaseOption.hpp"
#include <iomanip>      // std::setprecision

class VectorOption : public BaseOption
{

    int *m_Number = new int(0);
    std::vector<const char*> m_Vector;
    std::string m_Suffix;
public:
    VectorOption()
        : BaseOption(eOptionType::eVector)
    {
        m_XOffset = 26;
    }
    ~VectorOption() override = default;

    bool keyChecks() override
    {
        std::ostringstream stream;
        if (ClassPointers::g_KeyManager->getKeyState("Menu Right")) {

            if (*m_Number + 1 >= int(m_Vector.size()))
                *m_Number = 0;
            else
                *m_Number = *m_Number + 1;

            stream << m_Vector.at(*m_Number) << m_Suffix << "~c~ [" << *m_Number + 1 << "/" << m_Vector.size() << "]";
            std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        }
        if (ClassPointers::g_KeyManager->getKeyState("Menu Left"))
        {
            if (*m_Number - 1 < 0)
                *m_Number = m_Vector.size() - 1;
            else
                *m_Number = *m_Number - 1;

            stream << m_Vector.at(*m_Number) << m_Suffix << "~c~ [" << *m_Number + 1 << "/" << m_Vector.size() << "]";
            std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        }
        return true;
    };

    VectorOption& addLeft(const char* text)
    {
        std::strncpy(m_LeftText, text, sizeof m_LeftText);
        return *this;
    };

    VectorOption& addDesc(const char* text)
    {
        strncpy(m_Descrtiption, text, sizeof m_Descrtiption);
        return *this;
    };

    VectorOption& addSuffix(const char* suffix)
    {
        std::ostringstream stream;
        m_Suffix = suffix;
        stream << m_Vector.at(*m_Number) << m_Suffix << "~c~ [" << *m_Number + 1 << "/" << m_Vector.size() << "]";
        std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        return *this;
    }
    VectorOption& addVector(std::vector<const char*> &var)
    {
        std::ostringstream stream;
        m_Vector = var;
        stream.clear();
        if(m_Number)
            stream << m_Vector.at(*m_Number) << m_Suffix << "~c~ [" << *m_Number + 1 << "/" << m_Vector.size() << "]";
        else
            stream << m_Vector.at(0) << m_Suffix << "~c~ [" <<  1 << "/" << m_Vector.size() << "]";
        strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        stream.clear();
        return *this;
    }

    VectorOption& addVar(int& var)
    {
        std::ostringstream stream;
        m_Number = &var;
        stream.clear();
        stream << m_Vector.at(*m_Number) << m_Suffix << " ~c~ [" << *m_Number + 1 << "/" << m_Vector.size() << "]";
        strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        stream.clear();
        return *this;
    }

    VectorOption& addFunction(std::function<void()>&& func)
    {
        m_Function = std::move(func);
        return *this;
    }
};


