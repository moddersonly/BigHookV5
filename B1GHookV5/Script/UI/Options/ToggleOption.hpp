#pragma once
class ToggleOption : public BaseOption
{
    bool* m_Bool;
public:
	ToggleOption()
		: BaseOption(eOptionType::eBool)
	{
		std::strncpy(m_RightText, "~r~OFF", sizeof(m_RightText));
	}
    ~ToggleOption() override = default;

	bool keyChecks() override
	{
		if (ClassPointers::g_KeyManager->getKeyState("Menu Select"))
		{
			*m_Bool ^= true;
			std::strncpy(m_RightText, *m_Bool ? "~g~ON" : "~r~OFF", sizeof(m_RightText));
		}

		return true;
	}

	ToggleOption& addLeft(const char* text)
	{
		strncpy(m_LeftText, text, sizeof(m_LeftText));
		return *this;
	}

	ToggleOption& addDesc(const char* text)
	{
		std::strncpy(m_Descrtiption, text, sizeof(m_Descrtiption));
		return *this;
	}

    ToggleOption& addVar(bool& var)
	{
		m_Bool = &var;
		std::strncpy(m_RightText, *m_Bool ? "~g~ON" : "~r~OFF", sizeof(m_RightText));
		return *this;
	}

	ToggleOption& addFunction(std::function<void()>&& func)
	{
		m_Function = std::move(func);
		return *this;
	}
};


