#pragma once
#include "Options/BaseOption.hpp"
#include "Sub.hpp"

class MenuHandler
{
	std::vector<std::unique_ptr<Sub>> m_AllSubmenus{};
	std::stack<Sub*> m_SubmenuStack{};

	BigVec2 m_MenuCoords = { 1200.f, 200.f };
    float m_MenuWidth = 430.f;
    float m_MenuTitleTextOffsetY = -30.f;
    float m_OptionTextOffsetY = m_MenuCoords.y + 90.f;
    float m_OptionRectOffsetY = m_MenuCoords.y + 102.f;
    float m_OptionTextYMultiplier = 40.f;
    float m_TextWidthDivision = 2.085f;
    float m_SpriteWidthDivision = 2.2f;

    char m_OptionCount[32];
    char m_FrameCount[16];
    
    bool m_MenuOpen = false;
    std::size_t m_MaxOptions = 14;

    void renderHeader();
    void renderFooter() const;
    void renderOptions();
public:
    MenuHandler() = default;
    ~MenuHandler() = default;

    void newSub(Sub&& submenu);
    void handleSwitch(const char *subName, bool pushBack = true);
    void checkKeys();
    void renderMenu();
    void closeOpen(bool open);
    const char* frameCount();
    bool* isOpen()
    {
        return &m_MenuOpen;
    }
};

