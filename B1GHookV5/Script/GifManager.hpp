#pragma once
struct MenuGif
{
    MenuGif() = default;
    MenuGif(const char* ro0t, const int framec0unt, const int startfr4me, const int currentfr4me)
    {
        root = ro0t;
        framecount = framec0unt;
        startframe = startfr4me;
        currentframe = currentfr4me;
    }

    const char* root;

    int framecount;
    int startframe;
    int currentframe;
};

class GifManager
{
private:
    std::map<const char*, MenuGif> m_Map;
    std::vector<const char*> m_Roots;
public:
	GifManager() {}
	~GifManager() {}

    void pushGif(const char *root, const int frames, const int start = 0);
    void updateFrames();

	template <std::size_t bufferSize>
	void getFrame(const char* root, char(&buf)[bufferSize])
	{
		std::snprintf(buf, bufferSize - 1, "%s%i", m_Map[root].root, m_Map[root].currentframe);
	}
};

