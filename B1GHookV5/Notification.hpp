#pragma once
#include <string>

enum class eNotificationType
{
	eRegular,
	eClan1,
	eClan2
};
class Notification
{
	std::string m_IconDict;
	std::string m_IconTex;
	std::string m_Title;
	std::string m_SubTitle;
	std::string m_ClanTag;
	std::string m_Description;
	eNotificationType m_Type = eNotificationType::eRegular;
public:
	void setDict(const std::string& dict);

	void setTexture(const std::string& texture);

	void setTitle(const std::string& title);

	void setSubTItle(const std::string& sub);

	void setClanTag(const std::string& tag);

	void setDescription(const std::string& desc);

	void setType(const eNotificationType type);

	void open() const;
};

