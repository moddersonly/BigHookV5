#include "pch.hpp"
#include "NativeInvoker.hpp"

NativeInvoker::NativeHash errorExcludes[] =
{
	0xDFA2EF8E04127DD5
};

template <typename T, std::size_t Size>
bool findInArray(T value, T(&arr)[Size], std::size_t& out)
{
	for (std::size_t i = 0; i < Size; ++i)
	{
		if (arr[i] == value)
		{
			out = i;
			return true;
		}
	}

	return false;
}

void(*NativeCallContext::vectorFixFunction)(NativeCallContext* dis);
NativeInvoker g_NativeInvoker;

NativeInvoker::EntrypointFunction* NativeInvoker::getHandler(NativeInvoker::NativeHash hash)
{
	static std::uint64_t(&selectedOffsets)[NUM_NATIVES] = ClassPointers::g_ScriptThread->isSteam() ? steamEntrypoints : scEntrypoints;

	std::size_t index;
	if (findInArray(hash, nativeHashes, index))
	{
		return mem::module::main().add(selectedOffsets[index]).as<EntrypointFunction*>();
	}
	else
	{
		return nullptr;
	}
}

void NativeInvoker::initialize()
{
	m_ContextRetn = std::make_unique<NativeCallContext::Value[]>(NativeCallContext::maxRetnCount);
	m_ContextArgs = std::make_unique<NativeCallContext::Value[]>(NativeCallContext::maxArgCount);
	m_Context = std::make_unique<NativeCallContext>();
	m_Context->retn = m_ContextRetn.get();
	m_Context->args = m_ContextArgs.get();
	NativeCallContext::vectorFixFunction = "48 8D 4D 97 E8 ? ? ? ? 45 2B F4"_Scan.add(5).rip(4).as<decltype(NativeCallContext::vectorFixFunction)>();
}

void NativeInvoker::beginCall()
{
	m_Context->reset();
}

void NativeInvoker::endCall(NativeInvoker::NativeHash hash)
{
	EXCEPTION_POINTERS* expc = nullptr;
	auto handler = getHandler(hash);

	__try
	{
		handler(m_Context.get());
	}
	__except (expc = GetExceptionInformation(), EXCEPTION_EXECUTE_HANDLER)
	{
		std::size_t index;
		if (!findInArray(hash, errorExcludes, index))
		{
			ClassPointers::g_Logger->msg("Failed calling native %llx, exception code: %x, exception address: %p", hash, expc->ExceptionRecord->ExceptionCode, expc->ExceptionRecord->ExceptionAddress);
		}
	}
}
