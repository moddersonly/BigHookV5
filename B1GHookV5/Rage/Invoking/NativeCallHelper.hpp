#pragma once
#include "NativeInvoker.hpp"

template <typename R>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28, typename T29>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28, T29 P29)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->push(P29);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28, typename T29, typename T30>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28, T29 P29, T30 P30)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->push(P29);
	ClassPointers::g_NativeInvoker->push(P30);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28, typename T29, typename T30, typename T31>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28, T29 P29, T30 P30, T31 P31)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->push(P29);
	ClassPointers::g_NativeInvoker->push(P30);
	ClassPointers::g_NativeInvoker->push(P31);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28, typename T29, typename T30, typename T31, typename T32>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28, T29 P29, T30 P30, T31 P31, T32 P32)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->push(P29);
	ClassPointers::g_NativeInvoker->push(P30);
	ClassPointers::g_NativeInvoker->push(P31);
	ClassPointers::g_NativeInvoker->push(P32);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21, typename T22, typename T23, typename T24, typename T25, typename T26, typename T27, typename T28, typename T29, typename T30, typename T31, typename T32, typename T33>
static FORCEINLINE R invoke(NativeInvoker::NativeHash hash, T1 P1, T2 P2, T3 P3, T4 P4, T5 P5, T6 P6, T7 P7, T8 P8, T9 P9, T10 P10, T11 P11, T12 P12, T13 P13, T14 P14, T15 P15, T16 P16, T17 P17, T18 P18, T19 P19, T20 P20, T21 P21, T22 P22, T23 P23, T24 P24, T25 P25, T26 P26, T27 P27, T28 P28, T29 P29, T30 P30, T31 P31, T32 P32, T33 P33)
{
	ClassPointers::g_NativeInvoker->beginCall();
	ClassPointers::g_NativeInvoker->push(P1);
	ClassPointers::g_NativeInvoker->push(P2);
	ClassPointers::g_NativeInvoker->push(P3);
	ClassPointers::g_NativeInvoker->push(P4);
	ClassPointers::g_NativeInvoker->push(P5);
	ClassPointers::g_NativeInvoker->push(P6);
	ClassPointers::g_NativeInvoker->push(P7);
	ClassPointers::g_NativeInvoker->push(P8);
	ClassPointers::g_NativeInvoker->push(P9);
	ClassPointers::g_NativeInvoker->push(P10);
	ClassPointers::g_NativeInvoker->push(P11);
	ClassPointers::g_NativeInvoker->push(P12);
	ClassPointers::g_NativeInvoker->push(P13);
	ClassPointers::g_NativeInvoker->push(P14);
	ClassPointers::g_NativeInvoker->push(P15);
	ClassPointers::g_NativeInvoker->push(P16);
	ClassPointers::g_NativeInvoker->push(P17);
	ClassPointers::g_NativeInvoker->push(P18);
	ClassPointers::g_NativeInvoker->push(P19);
	ClassPointers::g_NativeInvoker->push(P20);
	ClassPointers::g_NativeInvoker->push(P21);
	ClassPointers::g_NativeInvoker->push(P22);
	ClassPointers::g_NativeInvoker->push(P23);
	ClassPointers::g_NativeInvoker->push(P24);
	ClassPointers::g_NativeInvoker->push(P25);
	ClassPointers::g_NativeInvoker->push(P26);
	ClassPointers::g_NativeInvoker->push(P27);
	ClassPointers::g_NativeInvoker->push(P28);
	ClassPointers::g_NativeInvoker->push(P29);
	ClassPointers::g_NativeInvoker->push(P30);
	ClassPointers::g_NativeInvoker->push(P31);
	ClassPointers::g_NativeInvoker->push(P32);
	ClassPointers::g_NativeInvoker->push(P33);
	ClassPointers::g_NativeInvoker->endCall(hash);
	return ClassPointers::g_NativeInvoker->getReturnValue<R>();
}

