#ifndef HOOKMANAGER_HPP
#define HOOKMANAGER_HPP

enum class eHookResult
{
	eHookOk,
	eHookFailed
};
class HookManager
{
	/**
	 * \brief Stores hooks for later unhooking,
	 */
	std::vector<LPVOID> m_Hooks{};
public:
	/**
	* \brief Constructs Class.
	*/
	HookManager();
	HookManager(const HookManager &obj) = default;
	HookManager &operator=(const HookManager &obj) = default;
	HookManager(HookManager &&obj) = default;
	HookManager &operator=(HookManager &&obj) = default;
	/**
	 * \brief Deconstructs Class.
	 */
	~HookManager();

	 /**
	 * \brief JMP Hook.
	 * \param function Function to detour.
	 * \param detour Function that will be jumped to.
	 * \param original Original Funciton (tells when to return to regular function);
	 * \param name The name of the hook. Used for debugging purposes.
	 * \return eHookOk / eHookFailed.
	 */

    eHookResult hookDetour(void *function, void *detour, void **original, const char* name);
    void applyQueue();
    /**
	* \brief unhooks all hooks
	*/
	void unhookAll();
	
};
#endif // HOOKMANAGER_HPP
